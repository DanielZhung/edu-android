package cn.zxd.edu.util;

import com.avos.avoscloud.AVUser;

public class AVUtils {

    public static boolean isAdministrator(AVUser user) {
        return user.getInt("Type") == 1;
    }

    public static boolean isSuperAdministrator(AVUser user) {
        return user.getInt("Type") == 2;
    }

}
