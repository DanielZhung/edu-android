package cn.zxd.edu.util;

import android.text.TextUtils;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

public class SuperHttpUtil {

    public static String HttpServerIp = null;

    private static AsyncHttpClient asyncHttpClient = new AsyncHttpClient(8080);

    public static void get(String url, TextHttpResponseHandler handler) {
        if (TextUtils.isEmpty(HttpServerIp)) {
            handler.onFailure(500, null, "", new Throwable("服务器异常"));
        } else {
            asyncHttpClient.get(HttpServerIp + url, handler);
        }
    }

    public static void post(String url, RequestParams params, TextHttpResponseHandler handler) {
        if (TextUtils.isEmpty(HttpServerIp)) {
            handler.onFailure(500, null, "", new Throwable("服务器异常"));
        } else {
            asyncHttpClient.post(HttpServerIp + url, params, handler);
        }
    }

}
