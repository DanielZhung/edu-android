package cn.zxd.edu.manager;

import com.avos.avoscloud.AVCloudQueryResult;
import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.CloudQueryCallback;
import com.avos.avoscloud.FindCallback;

import java.util.ArrayList;
import java.util.List;

import cn.zxd.edu.bean.User;
import cn.zxd.edu.bean.UserDeviceAuthorize;

public class UserManager {
    private static UserManager ourInstance = null;

    private static final Object locker = new Object();

    private List<AVObject> authorizedUser = null;

    public static UserManager getInstance() {
        if (ourInstance == null) {
            synchronized (locker) {
                if (ourInstance == null) {
                    ourInstance = new UserManager();
                }
            }
        }
        return ourInstance;
    }

    private UserManager() {
    }

    public void getUnauthorizedUser(int schoolIndex, final FindCallback<AVUser> callback) {
        AVQuery<AVUser> userAVQuery = new AVQuery<>("_User");
        userAVQuery.whereEqualTo("SchoolIndex", schoolIndex);
        userAVQuery.findInBackground(new FindCallback<AVUser>() {
            @Override
            public void done(List<AVUser> list, AVException e) {
                callback.done(list, e);
            }
        });
    }

    public void getAuthorizedUser(String deviceId, final FindCallback<AVObject> callback) {
        AVQuery<AVObject> relationQuery = new AVQuery<>("DeviceRelation");
        relationQuery.whereEqualTo("DeviceId", deviceId);
        relationQuery.findInBackground(new FindCallback<AVObject>() {
            @Override
            public void done(List<AVObject> list, AVException e) {
                if (authorizedUser == null)
                    authorizedUser = new ArrayList<>();
                authorizedUser.clear();
                authorizedUser.addAll(list);
                callback.done(authorizedUser, e);
            }
        });
    }

    public boolean notAuthorized(AVUser user) {
        for (AVObject avUser : authorizedUser) {
            if (avUser.getString("UserMobile").equals(user.getMobilePhoneNumber())) {
                return false;
            }
        }
        return true;
    }

    public void authorizeUser(String deviceId, List<User> users) {
        for (User user : users) {
            if (user.isChecked()) {
                AVObject object = new AVObject("DeviceRelation");
                object.put("DeviceId", deviceId);
                object.put("UserMobile", user.getMobile());
                object.put("UserNickname", user.getNickname());
                object.saveInBackground();
            }
        }
    }

    public void deleteAuthorize(UserDeviceAuthorize authorize, CloudQueryCallback<AVCloudQueryResult> callback) {
        String sql = String.format("DELETE FROM DeviceRelation WHERE objectId = \'%s\'", authorize.getObjectId());
        AVQuery.doCloudQueryInBackground(sql, callback);
    }

    public void deleteAllAuthorize(String deviceId) {
        String sql = String.format("DELETE FROM DeviceRelation WHERE DeviceId = \'%s\'", deviceId);
        AVQuery.doCloudQueryInBackground(sql, null);
    }
}
