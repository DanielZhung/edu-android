package cn.zxd.edu.manager;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.FindCallback;
import com.avos.avoscloud.GetCallback;
import com.avos.avoscloud.SaveCallback;

import java.util.ArrayList;
import java.util.List;

import cn.zxd.edu.bean.Device;

public class DeviceManager {
    private static DeviceManager ourInstance = null;
    private static final Object locker = new Object();

    private List<AVObject> bindDevices;

    private List<AVObject> unbindDevices;

    private List<AVObject> userDevices;

    public static DeviceManager getInstance() {
        if (ourInstance == null) {
            synchronized (locker) {
                if (ourInstance == null) {
                    ourInstance = new DeviceManager();
                }
            }
        }
        return ourInstance;
    }

    private DeviceManager() {
    }

    public void getBindDeviceList(FindCallback<AVObject> callback, int schoolIndex) {
        getBindDeviceList(callback, schoolIndex, false);
    }

    public void getBindDeviceList(final FindCallback<AVObject> callback, int schoolIndex, boolean forceQuery) {
        if (!forceQuery && bindDevices != null) {
            callback.done(bindDevices, null);
        } else {
            AVQuery<AVObject> deviceQuery = new AVQuery<>("Device").whereEqualTo("Added", true).whereEqualTo("SchoolIndex", schoolIndex);
            deviceQuery.findInBackground(new FindCallback<AVObject>() {
                @Override
                public void done(List<AVObject> list, AVException e) {
                    if (bindDevices == null)
                        bindDevices = new ArrayList<>();
                    bindDevices.clear();
                    bindDevices.addAll(list);
                    callback.done(bindDevices, e);
                }
            });
        }
    }

    public void getUnbindDeviceList(FindCallback<AVObject> callback, int schoolIndex) {

    }

    public void getUnbindDeviceList(final FindCallback<AVObject> callback, int schoolIndex, boolean forceQuery) {
        if (!forceQuery && unbindDevices != null) {
            callback.done(unbindDevices, null);
        } else {
            AVQuery<AVObject> deviceQuery = new AVQuery<>("Device").whereEqualTo("SchoolIndex", schoolIndex).whereEqualTo("Added", false).whereEqualTo("Online", true);
            deviceQuery.findInBackground(new FindCallback<AVObject>() {
                @Override
                public void done(List<AVObject> list, AVException e) {
                    if (unbindDevices == null)
                        unbindDevices = new ArrayList<>();
                    unbindDevices.clear();
                    unbindDevices.addAll(list);
                    callback.done(unbindDevices, e);
                }
            });
        }
    }

    public void getDeviceListByUser(FindCallback<AVObject> callback, String userMobile) {

    }

    public void getDeviceListByUser(final FindCallback<AVObject> callback, final String userMobile, boolean forceQuery) {
        if (!forceQuery && userDevices != null) {
            callback.done(userDevices, null);
        } else {
            AVQuery<AVObject> relationQuery = new AVQuery<>("DeviceRelation");
            relationQuery.whereEqualTo("UserMobile", userMobile);
            relationQuery.findInBackground(new FindCallback<AVObject>() {
                @Override
                public void done(List<AVObject> list, AVException e) {
                    if (e == null && list != null && list.size() > 0) {
                        List<String> ids = new ArrayList<>();
                        for (AVObject object : list) {
                            ids.add(object.getString("DeviceId"));
                        }
                        AVQuery<AVObject> deviceQuery = new AVQuery<>("Device");
                        deviceQuery.whereContainedIn("objectId", ids);
                        deviceQuery.findInBackground(new FindCallback<AVObject>() {
                            @Override
                            public void done(List<AVObject> deviceList, AVException e) {
                                if (userDevices == null)
                                    userDevices = new ArrayList<>();
                                userDevices.clear();
                                userDevices.addAll(deviceList);
                                callback.done(userDevices, e);
                            }
                        });
                    } else {
                        callback.done(new ArrayList<AVObject>(), e);
                    }
                }
            });
        }
    }

    public void addDevice(Device device, final String description) {
        AVQuery<AVObject> object = new AVQuery<>("Device");
        object.getInBackground(device.getDeviceId(), new GetCallback<AVObject>() {
            @Override
            public void done(AVObject avObject, AVException e) {
                avObject.put("Description", description);
                avObject.put("Added", true);
                avObject.saveInBackground();
            }
        });
    }

    public void deleteDevice(Device device, SaveCallback callback) {
        AVObject avObject = AVObject.createWithoutData("Device", device.getDeviceId());
        avObject.put("Added", false);
        avObject.saveInBackground(callback);
    }

    public void getDeviceInfo(Device device, GetCallback<AVObject> callback) {
        AVQuery<AVObject> object = new AVQuery<>("Device");
        object.getInBackground(device.getDeviceId(), callback);
    }
}
