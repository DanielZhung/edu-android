package cn.zxd.edu.manager;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.FindCallback;

import java.util.ArrayList;
import java.util.List;

public class SchoolManager {
    private static SchoolManager ourInstance = null;
    private static final Object locker = new Object();

    private List<AVObject> schoolList = null;

    public static SchoolManager getInstance() {
        if (ourInstance == null) {
            synchronized (locker) {
                if (ourInstance == null) {
                    ourInstance = new SchoolManager();
                }
            }
        }
        return ourInstance;
    }

    private SchoolManager() {
    }

    public void getSchoolList(FindCallback<AVObject> callback) {
        getSchoolList(callback, false);
    }

    public void getSchoolList(final FindCallback<AVObject> callback, boolean forceQuery) {
        if (!forceQuery && schoolList != null) {
            callback.done(schoolList, null);
        } else {
            final AVQuery<AVObject> schoolQuery = new AVQuery<>("School");
            schoolQuery.findInBackground(new FindCallback<AVObject>() {
                @Override
                public void done(List<AVObject> list, AVException e) {
                    if (schoolList == null)
                        schoolList = new ArrayList<>();
                    schoolList.clear();
                    schoolList.addAll(list);
                    callback.done(schoolList, e);
                }
            });
        }
    }
}
