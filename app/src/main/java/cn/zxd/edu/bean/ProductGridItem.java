package cn.zxd.edu.bean;

public class ProductGridItem {

    private String name;
    private int drawableRes;
    private String url;

    public ProductGridItem() {
    }

    public ProductGridItem(String name, int drawableRes, String url) {
        this.name = name;
        this.drawableRes = drawableRes;
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDrawableRes() {
        return drawableRes;
    }

    public void setDrawableRes(int drawableRes) {
        this.drawableRes = drawableRes;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
