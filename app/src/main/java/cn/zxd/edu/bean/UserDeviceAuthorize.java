package cn.zxd.edu.bean;

public class UserDeviceAuthorize {

    private String objectId;
    private String deviceId;
    private String userMobile;
    private String userNickname;

    public UserDeviceAuthorize() {
    }

    public UserDeviceAuthorize(String objectId, String deviceId, String userMobile, String userNickname) {
        this.objectId = objectId;
        this.deviceId = deviceId;
        this.userMobile = userMobile;
        this.userNickname = userNickname;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getUserMobile() {
        return userMobile;
    }

    public void setUserMobile(String userMobile) {
        this.userMobile = userMobile;
    }

    public String getUserNickname() {
        return userNickname;
    }

    public void setUserNickname(String userNickname) {
        this.userNickname = userNickname;
    }
}
