package cn.zxd.edu.bean;

import java.io.Serializable;

public class User implements Serializable {
    private String mobile;
    private String nickname;
    private boolean checked = false;

    public User(String mobile, String nickname) {
        this.mobile = mobile;
        this.nickname = nickname;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
}
