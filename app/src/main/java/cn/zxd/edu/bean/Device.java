package cn.zxd.edu.bean;

import java.io.Serializable;

public class Device implements Serializable {

    private String name;

    private String description;

    private boolean isOnline;

    private boolean isOn;

    private int schoolIndex;

    private boolean selected = false;

    private String deviceId;

    public Device() {
    }

    public Device(String name, String deviceId, String description, boolean isOnline, boolean isOn, int schoolIndex) {
        this.name = name;
        this.description = description;
        this.isOnline = isOnline;
        this.deviceId = deviceId;
        this.isOn = isOn;
        this.schoolIndex = schoolIndex;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isOnline() {
        return isOnline;
    }

    public void setOnline(boolean online) {
        isOnline = online;
    }

    public int getSchoolIndex() {
        return schoolIndex;
    }

    public void setSchoolIndex(int schoolIndex) {
        this.schoolIndex = schoolIndex;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public boolean isOn() {
        return isOn;
    }

    public void setOn(boolean on) {
        isOn = on;
    }
}
