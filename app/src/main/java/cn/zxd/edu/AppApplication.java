package cn.zxd.edu;

import android.app.Application;
import android.content.Context;

import com.avos.avoscloud.AVOSCloud;

public class AppApplication extends Application {

    private static Context appContext;

    @Override
    public void onCreate() {
        super.onCreate();
        appContext = getApplicationContext();
        AVOSCloud.initialize(appContext, "1j3OylkPSes1PwYeUoFmk3yu-gzGzoHsz","7n1od1bAN55d66nDrfeyHjMl");
        AVOSCloud.setDebugLogEnabled(true);
    }

    public static Context getAppContext() {
        return appContext;
    }
}
