package cn.zxd.edu.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.zxd.edu.R;

public class SelectorView extends ConstraintLayout {

    @BindView(R.id.v_top_line)
    View v_top_line;

    @BindView(R.id.v_bottom_line)
    View v_bottom_line;

    @BindView(R.id.iv_icon)
    ImageView iv_icon;

    @BindView(R.id.tv_text)
    TextView tv_text;

    @BindView(R.id.iv_right)
    ImageView iv_right;

    public SelectorView(Context context) {
        this(context, null);
    }

    public SelectorView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SelectorView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        LayoutInflater.from(context).inflate(R.layout.view_selector, this, true);
        ButterKnife.bind(this);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.SelectorView);
        iv_icon.setImageDrawable(typedArray.getDrawable(R.styleable.SelectorView_image));
        iv_right.setVisibility(typedArray.getBoolean(R.styleable.SelectorView_showRight, true) ? VISIBLE : INVISIBLE);
        tv_text.setText(typedArray.getText(R.styleable.SelectorView_text));
        v_top_line.setVisibility(typedArray.getBoolean(R.styleable.SelectorView_topLine, false) ? VISIBLE : INVISIBLE);
        v_bottom_line.setVisibility(typedArray.getBoolean(R.styleable.SelectorView_bottomLine, false) ? VISIBLE : INVISIBLE);
        typedArray.recycle();
    }
}
