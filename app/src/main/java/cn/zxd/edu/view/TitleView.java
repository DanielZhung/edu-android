package cn.zxd.edu.view;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.zxd.edu.R;

public class TitleView extends ConstraintLayout {

    @BindView(R.id.cl_titleViewRoot)
    ConstraintLayout cl_titleViewRoot;

    @BindView(R.id.iv_titleViewBack)
    ImageView iv_titleViewBack;

    @BindView(R.id.iv_titleViewRight)
    ImageView iv_titleViewRight;

    @BindView(R.id.tv_titleViewText)
    TextView tv_titleViewText;

    @BindView(R.id.tv_titleViewRight)
    TextView tv_titleViewRight;

    @BindView(R.id.v_divider_line)
    View v_divider;

    public interface OnLeftClickListener {
        void onLeftClick(View view);
    }

    public interface OnRightClickListener {
        void onRightClick(View view);
    }

    private OnLeftClickListener leftClickListener;

    private OnRightClickListener rightClickListener;

    public TitleView(Context context) {
        this(context, null);
    }

    public TitleView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TitleView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        LayoutInflater.from(context).inflate(R.layout.view_title, this, true);
        ButterKnife.bind(this);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.TitleView);
        //背景颜色
        if (typedArray.hasValue(R.styleable.TitleView_backgroundRes)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                cl_titleViewRoot.setBackground(typedArray.getDrawable(R.styleable.TitleView_backgroundRes));
            } else {
                cl_titleViewRoot.setBackgroundDrawable(typedArray.getDrawable(R.styleable.TitleView_backgroundRes));
            }
        } else {
            cl_titleViewRoot.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        }
        //返回按钮
        if (typedArray.hasValue(R.styleable.TitleView_showBack) && typedArray.getBoolean(R.styleable.TitleView_showBack, false)) {
            iv_titleViewBack.setVisibility(VISIBLE);
        } else {
            iv_titleViewBack.setVisibility(GONE);
        }
        //标题
        if (typedArray.hasValue(R.styleable.TitleView_titleText)) {
            tv_titleViewText.setText(typedArray.getText(R.styleable.TitleView_titleText));
        }
        //右文字
        if (typedArray.hasValue(R.styleable.TitleView_rightText)) {
            tv_titleViewRight.setText(typedArray.getText(R.styleable.TitleView_rightText));
        }
        //右图标
        if (typedArray.hasValue(R.styleable.TitleView_rightImage)) {
            iv_titleViewRight.setVisibility(VISIBLE);
            iv_titleViewRight.setImageDrawable(typedArray.getDrawable(R.styleable.TitleView_rightImage));
        } else {
            iv_titleViewRight.setVisibility(GONE);
        }
        //分割线
        v_divider.setVisibility(typedArray.getBoolean(R.styleable.TitleView_dividerLine, true) ? VISIBLE : GONE);
        typedArray.recycle();
    }

    @OnClick(R.id.iv_titleViewBack)
    protected void onLeftClick() {
        if (leftClickListener != null) {
            leftClickListener.onLeftClick(iv_titleViewBack);
        } else {
            ((Activity) getContext()).finish();
        }
    }

    @OnClick(value = {R.id.iv_titleViewRight, R.id.tv_titleViewRight})
    protected void onRightClick(View view) {
        if (rightClickListener != null) {
            rightClickListener.onRightClick(view);
        } else {
            ((Activity) getContext()).finish();
        }
    }

    public void setOnLeftClickListener(OnLeftClickListener leftClickListener) {
        this.leftClickListener = leftClickListener;
    }

    public void setOnRightClickListener(OnRightClickListener rightClickListener) {
        this.rightClickListener = rightClickListener;
    }

    public void setTitleText(CharSequence text) {
        tv_titleViewText.setText(text);
    }

    public void setRightImageRes(int imageRes) {
        if (imageRes > 0) {
            iv_titleViewRight.setImageResource(imageRes);
            iv_titleViewRight.setVisibility(VISIBLE);
        } else {
            iv_titleViewRight.setVisibility(GONE);
        }
    }

    public void setRightText(CharSequence text) {
        tv_titleViewRight.setText(text);
    }

}
