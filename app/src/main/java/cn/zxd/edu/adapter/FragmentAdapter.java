package cn.zxd.edu.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import cn.zxd.edu.ui.DeviceFragment;
import cn.zxd.edu.ui.FindFragment;
import cn.zxd.edu.ui.MeFragment;

public class FragmentAdapter extends FragmentPagerAdapter {

    private final Fragment[] fragments = new Fragment[]{new DeviceFragment(), new FindFragment(), new MeFragment()};

    public FragmentAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int i) {
        return fragments[i];
    }

    @Override
    public int getCount() {
        return fragments.length;
    }
}
