package cn.zxd.edu.adapter;

import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import cn.zxd.edu.R;

public class ImageViewAdapter extends PagerAdapter {

    private final int[] imageRes = new int[]{R.drawable.image2, R.drawable.image1, R.drawable.image3, R.drawable.image4};

    @Override
    public int getCount() {
        return imageRes.length;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        ImageView imageView = new ImageView(container.getContext());
        imageView.setImageResource(imageRes[position]);
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        container.addView(imageView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        return imageView;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view == o;
    }
}
