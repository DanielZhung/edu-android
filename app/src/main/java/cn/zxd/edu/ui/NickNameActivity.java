package cn.zxd.edu.ui;

import android.text.TextUtils;
import android.widget.EditText;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.SaveCallback;

import butterknife.BindView;
import butterknife.OnClick;
import cn.zxd.edu.R;

public class NickNameActivity extends BaseActivity {

    @BindView(R.id.et_nickname)
    EditText et_nickname;

    @Override
    public int layoutRes() {
        return R.layout.activity_nickname;
    }

    @OnClick(R.id.btn_nickname)
    protected void addNickname() {
        String nickname = et_nickname.getText().toString();
        if (TextUtils.isEmpty(nickname)) {
            showToast("请先填写昵称");
        } else {
            final AVUser user = AVUser.getCurrentUser();
            user.put("nickname", nickname);
            user.saveInBackground(new SaveCallback() {
                @Override
                public void done(AVException e) {
                    if (e == null) {
                        if (user.getInt("SchoolIndex") <= 0) {
                            startActivity(ChooseSchoolActivity.class);
                        }
                        finish();
                    } else {
                        showToast(e.getMessage());
                    }
                }
            });
        }
    }
}
