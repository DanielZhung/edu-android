package cn.zxd.edu.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.avos.avoscloud.AVCloudQueryResult;
import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.CloudQueryCallback;
import com.avos.avoscloud.FindCallback;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnItemLongClick;
import cn.zxd.edu.R;
import cn.zxd.edu.adapter.CommonAdapter;
import cn.zxd.edu.adapter.CommonViewHolder;
import cn.zxd.edu.bean.Device;
import cn.zxd.edu.bean.User;
import cn.zxd.edu.bean.UserDeviceAuthorize;
import cn.zxd.edu.manager.UserManager;
import cn.zxd.edu.view.CircleBarView;
import cn.zxd.edu.view.TitleView;

public class AuthorizeActivity extends BaseActivity {

    private static final int REQUEST_UNAUTHORIZED_USER = 102;

    @BindView(R.id.tv_authorize)
    TitleView tv_authorize;

    @BindView(R.id.lv_authorized_user)
    ListView lv_authorized_user;

    Device device = null;

    private boolean fromResult = false;

    private AlertDialog authorizeUserAlertDialog = null;

    List<UserDeviceAuthorize> authorizedUserMobiles = new ArrayList<>();

    @Override
    public int layoutRes() {
        return R.layout.activity_authorize;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        device = (Device) getIntent().getSerializableExtra("device");
        tv_authorize.setOnRightClickListener(new TitleView.OnRightClickListener() {
            @Override
            public void onRightClick(View view) {
                Intent intent = new Intent(activityThis, SelectUserPopupActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("device", device);
                intent.putExtras(bundle);
                startActivityForResult(intent, REQUEST_UNAUTHORIZED_USER);
                overridePendingTransition(0, 0);
            }
        });
        lv_authorized_user.setAdapter(new CommonAdapter<UserDeviceAuthorize>(activityThis, authorizedUserMobiles, R.layout.view_unbind_device_list) {
            @Override
            public void convert(CommonViewHolder helper, UserDeviceAuthorize item) {
                helper.setText(R.id.tv_unbind_device, item.getUserNickname() + "(" + item.getUserMobile() + ")");
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (fromResult) {
            fromResult = false;
        } else {
            UserManager.getInstance().getAuthorizedUser(device.getDeviceId(), new FindCallback<AVObject>() {
                @Override
                public void done(List<AVObject> list, AVException e) {
                    authorizedUserMobiles.clear();
                    for (AVObject object : list) {
                        authorizedUserMobiles.add(new UserDeviceAuthorize(object.getObjectId(), object.getString("DeviceId"), object.getString("UserMobile"), object.getString("UserNickname")));
                    }
                    ((BaseAdapter) lv_authorized_user.getAdapter()).notifyDataSetChanged();
                }
            });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_UNAUTHORIZED_USER && resultCode == RESULT_OK) {
            fromResult = true;
            List<User> users = new ArrayList<>();
            users.addAll((List<User>) data.getSerializableExtra("users"));
            authorizedUser(users);
        } else {
            fromResult = false;
        }
    }

    @OnItemLongClick(R.id.lv_authorized_user)
    protected boolean longClickToDeleteAuthorize(AdapterView<?> parent, View view, final int position, long id) {
        final UserDeviceAuthorize authorize = authorizedUserMobiles.get(position);
        new AlertDialog.Builder(activityThis).setTitle("提示").setMessage("是否取消授权给用户:" + authorize.getUserMobile())
                .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        UserManager.getInstance().deleteAuthorize(authorize, new CloudQueryCallback<AVCloudQueryResult>() {
                            @Override
                            public void done(AVCloudQueryResult avCloudQueryResult, AVException e) {
                                if (e == null) {
                                    authorizedUserMobiles.remove(position);
                                    ((BaseAdapter) lv_authorized_user.getAdapter()).notifyDataSetChanged();
                                }
                            }
                        });
                    }
                })
                .setNegativeButton("取消", null).create().show();
        return true;
    }

    private void authorizedUser(List<User> users) {
        UserManager.getInstance().authorizeUser(device.getDeviceId(), users);
        authorizeUserAlertDialog = new AlertDialog.Builder(activityThis).setView(R.layout.view_alert_dialog_process)
                .create();
        authorizeUserAlertDialog.setCancelable(false);
        authorizeUserAlertDialog.setCanceledOnTouchOutside(false);
        authorizeUserAlertDialog.show();
        Window window = authorizeUserAlertDialog.getWindow();
        assert window != null;
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));  // 有白色背景，加这句代码
        ((TextView) window.findViewById(R.id.tv_title)).setText("正在授权中");
        CircleBarView cbv_progress = window.findViewById(R.id.cbv_progress);
        TextView tv_percent = window.findViewById(R.id.tv_percent);
        final ImageView iv_done = window.findViewById(R.id.iv_done);
        cbv_progress.setTextView(tv_percent);
        cbv_progress.setProgressNum(100, 3000);
        cbv_progress.setOnAnimationListener(new CircleBarView.OnAnimationListener() {
            @Override
            public String howToChangeText(float interpolatedTime, float updateNum, float maxNum) {
                if (interpolatedTime < 1)
                    return (int) (interpolatedTime * updateNum / maxNum * 100) + "%";
                else {
                    iv_done.setVisibility(View.VISIBLE);
                    iv_done.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    authorizedDone();
                                }
                            });
                        }
                    }, 300);
                    return "";
                }
            }

            @Override
            public void howTiChangeProgressColor(Paint paint, float interpolatedTime, float updateNum, float maxNum) {

            }
        });
    }

    private void authorizedDone() {
        authorizeUserAlertDialog.dismiss();
        onResume();
    }
}
