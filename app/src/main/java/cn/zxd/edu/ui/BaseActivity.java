package cn.zxd.edu.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.avos.avoscloud.AVUser;

import butterknife.ButterKnife;

public abstract class BaseActivity extends AppCompatActivity {

    protected BaseActivity activityThis;

    public static final int REQUEST_CODE_FOR_LOGIN = 100;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        setContentView(layoutRes());
        activityThis = this;
        ButterKnife.bind(activityThis);
    }

    public abstract int layoutRes();

    public void startActivity(Class<? extends Activity> clazz) {
        startActivity(new Intent(activityThis, clazz));
    }

    public void startActivity(Class<? extends Activity> clazz, Bundle bundle) {
        startActivity(new Intent(activityThis, clazz).putExtras(bundle));
    }

    protected void showToast(CharSequence text) {
        Toast.makeText(activityThis, text, Toast.LENGTH_SHORT).show();
    }
}
