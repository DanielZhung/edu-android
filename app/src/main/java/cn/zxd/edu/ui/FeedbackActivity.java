package cn.zxd.edu.ui;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVFile;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.ProgressCallback;
import com.avos.avoscloud.SaveCallback;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import cn.zxd.edu.R;
import cn.zxd.edu.util.KeyboardUtils;
import cn.zxd.edu.util.RealPathFromUriUtils;
import cn.zxd.edu.view.CircleBarView;

public class FeedbackActivity extends BaseActivity {

    private static final int REQUEST_IMAGE = 103;
    private static final int REQUEST_VIDEO = 104;
    private static final int REQUEST_PERMISSION_IMAGE = 106;
    private static final int REQUEST_PERMISSION_VIDEO = 107;

    @BindView(R.id.et_feedback)
    EditText et_feedback;

    @BindView(R.id.tv_feedback_count)
    TextView tv_feedback_count;

    @BindView(R.id.iv_media1)
    ImageView iv_media1;

    @BindView(R.id.iv_media2)
    ImageView iv_media2;

    @BindView(R.id.iv_media3)
    ImageView iv_media3;

    @BindView(R.id.iv_media4)
    ImageView iv_media4;

    private int index = 0;

    private String imagePath1, imagePath2, imagePath3, videoPath4;

    private int state1 = 0, state2 = 0, state3 = 0, state4 = 0;

    private AlertDialog uploadAlertDialog = null;

    AVObject feedbackObject;

    @Override
    public int layoutRes() {
        return R.layout.activity_feedback;
    }

    @OnTextChanged(value = R.id.et_feedback, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    protected void afterTextChanged(Editable s) {
        tv_feedback_count.setText(String.format(Locale.getDefault(), "%d/200", s.toString().length()));
    }

    @OnClick(value = {R.id.rl_media1, R.id.rl_media2, R.id.rl_media3})
    protected void clickToAddImage(View view) {
        switch (view.getId()) {
            case R.id.rl_media1:
                index = 1;
                break;
            case R.id.rl_media2:
                index = 2;
                break;
            case R.id.rl_media3:
                index = 3;
                break;
        }
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
            int hasWriteStoragePermission = ContextCompat.checkSelfPermission(getApplication(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
            int hasReadStoragePermission = ContextCompat.checkSelfPermission(getApplication(), Manifest.permission.READ_EXTERNAL_STORAGE);
            if (hasWriteStoragePermission == PackageManager.PERMISSION_GRANTED && hasReadStoragePermission == PackageManager.PERMISSION_GRANTED) {
                //拥有权限，执行操作
                selectImage();
            } else {
                //没有权限，向用户请求权限
                ActivityCompat.requestPermissions(activityThis, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_PERMISSION_IMAGE);
            }
        } else {
            selectImage();
        }
    }

    @OnClick(R.id.rl_media4)
    protected void clickToAddVideo() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
            int hasWriteStoragePermission = ContextCompat.checkSelfPermission(getApplication(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
            int hasReadStoragePermission = ContextCompat.checkSelfPermission(getApplication(), Manifest.permission.READ_EXTERNAL_STORAGE);
            if (hasWriteStoragePermission == PackageManager.PERMISSION_GRANTED && hasReadStoragePermission == PackageManager.PERMISSION_GRANTED) {
                //拥有权限，执行操作
                selectVideo();
            } else {
                //没有权限，向用户请求权限
                ActivityCompat.requestPermissions(activityThis, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_PERMISSION_VIDEO);
            }
        } else {
            selectVideo();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_IMAGE:
                    if (data != null) {
                        Uri uri = data.getData();
                        if (uri != null) {
                            String path = RealPathFromUriUtils.getRealPathFromUri(this, data.getData());
                            Bitmap source = BitmapFactory.decodeFile(path);
                            Bitmap bitmap = ThumbnailUtils.extractThumbnail(source, 250, 250);
                            switch (index) {
                                case 1:
                                    imagePath1 = path;
                                    iv_media1.setImageBitmap(bitmap);
                                    break;
                                case 2:
                                    imagePath2 = path;
                                    iv_media2.setImageBitmap(bitmap);
                                    break;
                                case 3:
                                    imagePath3 = path;
                                    iv_media3.setImageBitmap(bitmap);
                                    break;
                                default:
                                    break;
                            }

                        }
                    }
                    break;
                case REQUEST_VIDEO:
                    if (data != null) {
                        Uri uri = data.getData();
                        if (uri != null) {
                            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
                            if (cursor != null) {
                                cursor.moveToFirst();
                                String path = cursor.getString(1);
                                videoPath4 = path;
                                Bitmap bitmap = ThumbnailUtils.createVideoThumbnail(path, MediaStore.Video.Thumbnails.MINI_KIND);
                                iv_media4.setVisibility(View.VISIBLE);
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                    iv_media4.setBackground(new BitmapDrawable(null, bitmap));
                                } else {
                                    iv_media4.setBackgroundDrawable(new BitmapDrawable(bitmap));
                                }
                                cursor.close();
                            } else {
                                showToast("找不到文件");
                            }
                        }
                    }
                    break;
            }
        }
    }

    ImageView iv_done = null;

    @OnClick(R.id.btn_feedback)
    protected void feedback() {
        if (et_feedback.getText().toString().length() > 0) {
            feedbackObject = new AVObject("Feedback");
            feedbackObject.put("UserMobile", AVUser.getCurrentUser().getMobilePhoneNumber());
            feedbackObject.put("Message", et_feedback.getText().toString());
            try {
                if (!TextUtils.isEmpty(imagePath1)) {
                    File file = new File(imagePath1);
                    AVFile avfile = AVFile.withFile(file.getName(), file);
                    feedbackObject.put("Image1", avfile);
                    avfile.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(AVException e) {
                            state1 = e == null ? 1 : -1;
                            if (state2 != 0 && state3 != 0 && state4 != 0) {
                                uploadDone();
                            }
                        }
                    });
                } else {
                    state1 = 1;
                }
                if (!TextUtils.isEmpty(imagePath2)) {
                    File file = new File(imagePath2);
                    AVFile avfile = AVFile.withFile(file.getName(), file);
                    feedbackObject.put("Image2", avfile);
                    avfile.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(AVException e) {
                            state2 = e == null ? 1 : -1;
                            if (state1 != 0 && state3 != 0 && state4 != 0) {
                                uploadDone();
                            }
                        }
                    });
                } else {
                    state2 = 1;
                }
                if (!TextUtils.isEmpty(imagePath3)) {
                    File file = new File(imagePath3);
                    AVFile avfile = AVFile.withFile(file.getName(), file);
                    feedbackObject.put("Image3", avfile);
                    avfile.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(AVException e) {
                            state3 = e == null ? 1 : -1;
                            if (state1 != 0 && state2 != 0 && state4 != 0) {
                                uploadDone();
                            }
                        }
                    });
                } else {
                    state3 = 1;
                }
                if (!TextUtils.isEmpty(videoPath4)) {
                    File file = new File(videoPath4);
                    AVFile avfile = AVFile.withFile(file.getName(), file);
                    feedbackObject.put("Video", avfile);
                    avfile.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(AVException e) {
                            state4 = e == null ? 1 : -1;
                            if (state1 != 0 && state2 != 0 && state3 != 0) {
                                uploadDone();
                            }
                        }
                    });
                } else {
                    state4 = 1;
                }
                uploadAlertDialog = new AlertDialog.Builder(activityThis).setView(R.layout.view_alert_dialog_process)
                        .create();
                uploadAlertDialog.setCancelable(false);
                uploadAlertDialog.setCanceledOnTouchOutside(false);
                uploadAlertDialog.show();
                Window window = uploadAlertDialog.getWindow();
                assert window != null;
                window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));  // 有白色背景，加这句代码
                ((TextView) window.findViewById(R.id.tv_title)).setText("正在上传中");
                CircleBarView cbv_progress = window.findViewById(R.id.cbv_progress);
                TextView tv_percent = window.findViewById(R.id.tv_percent);
                iv_done = window.findViewById(R.id.iv_done);
                cbv_progress.setTextView(tv_percent);
                cbv_progress.setProgressNum(100, 10000);
                cbv_progress.setOnAnimationListener(new CircleBarView.OnAnimationListener() {
                    @Override
                    public String howToChangeText(float interpolatedTime, float updateNum, float maxNum) {
                        if (interpolatedTime < 1)
                            return (int) (interpolatedTime * updateNum / maxNum * 100) + "%";
                        else {
                            if (state1 != 0 && state2 != 0 && state3 != 0 && state4 != 0) {
                                uploadDone();
                            }
                            return "";
                        }
                    }

                    @Override
                    public void howTiChangeProgressColor(Paint paint, float interpolatedTime, float updateNum, float maxNum) {

                    }
                });
            } catch (Exception e) {
                showToast(e.getMessage());
            }
        } else {
            showToast("请写一些意见吧");
        }
    }

    private void selectImage() {
        KeyboardUtils.closeKeybord(et_feedback, activityThis);
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(intent, REQUEST_IMAGE);
    }

    private void selectVideo() {
        KeyboardUtils.closeKeybord(et_feedback, activityThis);
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("video/*");
        startActivityForResult(intent, REQUEST_VIDEO);
    }

    private void uploadDone() {
        feedbackObject.saveInBackground(new SaveCallback() {
            @Override
            public void done(AVException e) {
                if (e == null) {
                    if (iv_done != null) {
                        iv_done.setVisibility(View.VISIBLE);
                        iv_done.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                uploadAlertDialog.dismiss();
                                startActivity(FeedbackDoneActivity.class);
                                finish();
                            }
                        }, 200);
                    }
                }
            }
        });
    }
}
