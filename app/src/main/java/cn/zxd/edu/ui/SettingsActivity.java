package cn.zxd.edu.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.avos.avoscloud.AVUser;

import butterknife.BindView;
import butterknife.OnClick;
import cn.zxd.edu.R;

public class SettingsActivity extends BaseActivity {

    @BindView(R.id.btn_logout)
    Button sv_logout;

    @BindView(R.id.tv_mobile)
    TextView tv_mobile;

    @Override
    public int layoutRes() {
        return R.layout.activity_settings;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (AVUser.getCurrentUser() != null) {
            sv_logout.setVisibility(View.VISIBLE);
            String mobile = AVUser.getCurrentUser().getMobilePhoneNumber();
            String hideMobile = mobile.substring(0, 3) + "****" + mobile.substring(7);
            tv_mobile.setText(hideMobile);
        } else {
            sv_logout.setVisibility(View.GONE);
            tv_mobile.setText("");
        }
    }

    @OnClick(R.id.btn_logout)
    protected void logout() {
        AVUser.logOut();
        finish();
    }

    @OnClick(R.id.rl_clear_cache)
    protected void clearCache() {
        showToast("已清除缓存");
    }

    @OnClick(R.id.rl_check_update)
    protected void checkUpdate() {
        showToast("当前已经是最新版本");
    }
}
