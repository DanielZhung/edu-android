package cn.zxd.edu.ui;

import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseAdapter;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.FindCallback;

import java.util.List;

import cn.zxd.edu.R;
import cn.zxd.edu.adapter.CommonAdapter;
import cn.zxd.edu.adapter.CommonViewHolder;
import cn.zxd.edu.bean.Device;
import cn.zxd.edu.manager.DeviceManager;

public class ReportDevicePopupActivity extends SelectDevicePopupActivity {

    @Override
    protected void getDevice() {
        DeviceManager.getInstance().getBindDeviceList(new FindCallback<AVObject>() {
            @Override
            public void done(List<AVObject> list, AVException e) {
                if (e == null) {
                    unbindDevices.clear();
                    for (AVObject object : list) {
                        Device device = new Device(object.getString("Name"), object.getObjectId(), object.getString("Description"), object.getBoolean("Online"), object.getBoolean("On"), object.getInt("SchoolIndex"));
                        if (selectDevice != null && device.getName().equals(selectDevice.getName())) {
                            device.setSelected(true);
                        }
                        unbindDevices.add(device);
                    }
                    ((BaseAdapter) lv_unbind_devices.getAdapter()).notifyDataSetChanged();
                } else {
                    showToast(e.getMessage());
                }
            }
        }, AVUser.getCurrentUser().getInt("SchoolIndex"), true);
    }

    @Override
    protected void initAdapter() {
        lv_unbind_devices.setAdapter(new CommonAdapter<Device>(activityThis, unbindDevices, R.layout.view_unbind_device_list) {
            @Override
            public void convert(CommonViewHolder helper, Device item) {
                helper.setText(R.id.tv_unbind_device, item.getDescription() + "(" + item.getName() + ")");
                helper.getView(R.id.iv_device_select).setVisibility(item.isSelected() ? View.VISIBLE : View.GONE);
            }
        });
    }
}
