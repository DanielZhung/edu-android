package cn.zxd.edu.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.ImageView;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.FindCallback;

import java.util.List;

import butterknife.BindView;
import cn.zxd.edu.R;
import cn.zxd.edu.manager.SchoolManager;
import cn.zxd.edu.util.SuperHttpUtil;

public class SplashActivity extends BaseActivity {

    @BindView(R.id.iv_icon)
    ImageView iv_icon;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    if (AVUser.getCurrentUser() != null) {
                        AVUser.getCurrentUser().fetch();
                        if (AVUser.getCurrentUser().getInt("SchoolIndex") > 0) {
                            SchoolManager.getInstance().getSchoolList(new FindCallback<AVObject>() {
                                @Override
                                public void done(List<AVObject> list, AVException e) {
                                    for (AVObject object : list) {
                                        if (object.getInt("Index") == AVUser.getCurrentUser().getInt("SchoolIndex")) {
                                            SuperHttpUtil.HttpServerIp = "http://" + object.getString("ServerIP");
                                        }
                                    }
                                }
                            });
                        }
                    }
                } catch (AVException e) {
                    e.printStackTrace();
                }
            }
        }).start();
        iv_icon.postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(MainActivity.class);
                finish();
            }
        }, 1000);
    }

    @Override
    public int layoutRes() {
        return R.layout.activity_splash;
    }
}
