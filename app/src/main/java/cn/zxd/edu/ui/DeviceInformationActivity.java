package cn.zxd.edu.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import butterknife.BindView;
import cn.zxd.edu.R;
import cn.zxd.edu.view.Html5WebView;

public class DeviceInformationActivity extends BaseActivity {

    @BindView(R.id.wv_html5)
    Html5WebView wv_html5;

    @Override
    public int layoutRes() {
        return R.layout.activity_information;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String url = getIntent().getExtras().getString("url");
        if (!TextUtils.isEmpty(url)) {
            wv_html5.loadUrl(url);
        }
    }
}
