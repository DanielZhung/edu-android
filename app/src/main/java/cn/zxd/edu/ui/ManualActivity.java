package cn.zxd.edu.ui;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ViewFlipper;

import butterknife.BindView;
import butterknife.OnClick;
import cn.zxd.edu.R;

public class ManualActivity extends BaseActivity {

    @BindView(R.id.tv_1)
    TextView tv_1;

    @BindView(R.id.tv_2)
    TextView tv_2;

    @BindView(R.id.v_mark_1)
    View v_mark_1;

    @BindView(R.id.v_mark_2)
    View v_mark_2;

    @BindView(R.id.vf_container)
    ViewFlipper vf_container;


    @Override
    public int layoutRes() {
        return R.layout.activity_manual;
    }

    @OnClick(R.id.tv_1)
    protected void clickToSenior() {
        tv_1.setTextColor(0xFF333333);
        tv_2.setTextColor(0xFFABABAB);
        v_mark_1.setVisibility(View.VISIBLE);
        v_mark_2.setVisibility(View.GONE);
        if (vf_container.getCurrentView().getId() != R.id.sv_senior)
            vf_container.showPrevious();
    }

    @OnClick(R.id.tv_2)
    protected void clickToJunior() {
        tv_2.setTextColor(0xFF333333);
        tv_1.setTextColor(0xFFABABAB);
        v_mark_2.setVisibility(View.VISIBLE);
        v_mark_1.setVisibility(View.GONE);
        if (vf_container.getCurrentView().getId() != R.id.sv_junior)
            vf_container.showNext();
    }

    @OnClick({R.id.ll_s_1, R.id.ll_s_2, R.id.ll_s_3, R.id.ll_s_4, R.id.ll_s_5, R.id.ll_s_6, R.id.ll_s_7,
            R.id.ll_j_1, R.id.ll_j_2, R.id.ll_j_3, R.id.ll_j_4, R.id.ll_j_5, R.id.ll_j_6, R.id.ll_j_7, R.id.ll_j_8})
    protected void clickToOpenDetail(View view) {
        ViewGroup viewGroup = null;
        ImageView imageView = null;
        switch (view.getId()) {
            case R.id.ll_s_1:
                viewGroup = findViewById(R.id.ll_s_1_detail);
                imageView = findViewById(R.id.iv_s_1);
                break;
            case R.id.ll_s_2:
                viewGroup = findViewById(R.id.ll_s_2_detail);
                imageView = findViewById(R.id.iv_s_2);
                break;
            case R.id.ll_s_3:
                viewGroup = findViewById(R.id.ll_s_3_detail);
                imageView = findViewById(R.id.iv_s_3);
                break;
            case R.id.ll_s_4:
                viewGroup = findViewById(R.id.ll_s_4_detail);
                imageView = findViewById(R.id.iv_s_4);
                break;
            case R.id.ll_s_5:
                viewGroup = findViewById(R.id.ll_s_5_detail);
                imageView = findViewById(R.id.iv_s_5);
                break;
            case R.id.ll_s_6:
                viewGroup = findViewById(R.id.ll_s_6_detail);
                imageView = findViewById(R.id.iv_s_6);
                break;
            case R.id.ll_s_7:
                viewGroup = findViewById(R.id.ll_s_7_detail);
                imageView = findViewById(R.id.iv_s_7);
                break;
            case R.id.ll_j_1:
                viewGroup = findViewById(R.id.ll_j_1_detail);
                imageView = findViewById(R.id.iv_j_1);
                break;
            case R.id.ll_j_2:
                viewGroup = findViewById(R.id.ll_j_2_detail);
                imageView = findViewById(R.id.iv_j_2);
                break;
            case R.id.ll_j_3:
                viewGroup = findViewById(R.id.ll_j_3_detail);
                imageView = findViewById(R.id.iv_j_3);
                break;
            case R.id.ll_j_4:
                viewGroup = findViewById(R.id.ll_j_4_detail);
                imageView = findViewById(R.id.iv_j_4);
                break;
            case R.id.ll_j_5:
                viewGroup = findViewById(R.id.ll_j_5_detail);
                imageView = findViewById(R.id.iv_j_5);
                break;
            case R.id.ll_j_6:
                viewGroup = findViewById(R.id.ll_j_6_detail);
                imageView = findViewById(R.id.iv_j_6);
                break;
            case R.id.ll_j_7:
                viewGroup = findViewById(R.id.ll_j_7_detail);
                imageView = findViewById(R.id.iv_j_7);
                break;
            case R.id.ll_j_8:
                viewGroup = findViewById(R.id.ll_j_8_detail);
                imageView = findViewById(R.id.iv_j_8);
                break;
        }
        if (viewGroup != null) {
            boolean isShow = viewGroup.getVisibility() == View.VISIBLE;
            viewGroup.setVisibility(isShow ? View.GONE : View.VISIBLE);
            imageView.setRotation(isShow ? 0 : 180);
        }
    }

}
