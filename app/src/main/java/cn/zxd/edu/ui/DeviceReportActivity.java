package cn.zxd.edu.ui;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVFile;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.FindCallback;
import com.avos.avoscloud.SaveCallback;

import java.io.File;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.zxd.edu.R;
import cn.zxd.edu.bean.Device;
import cn.zxd.edu.manager.SchoolManager;
import cn.zxd.edu.util.KeyboardUtils;
import cn.zxd.edu.util.RealPathFromUriUtils;
import cn.zxd.edu.view.CircleBarView;

public class DeviceReportActivity extends BaseActivity {

    @BindView(R.id.tv_school_name)
    TextView tv_school_name;

    @BindView(R.id.tv_device_name)
    TextView tv_device_name;

    @BindView(R.id.et_report)
    EditText et_report;

    @BindView(R.id.iv_media1)
    ImageView iv_media1;

    @BindView(R.id.iv_media2)
    ImageView iv_media2;

    @BindView(R.id.iv_media3)
    ImageView iv_media3;

    @BindView(R.id.iv_media4)
    ImageView iv_media4;

    Device device = null;

    private static final int REQUEST_IMAGE = 103;
    private static final int REQUEST_VIDEO = 104;
    private static final int REQUEST_BIND_DEVICE = 105;
    private static final int REQUEST_PERMISSION_IMAGE = 106;
    private static final int REQUEST_PERMISSION_VIDEO = 107;

    private int index = 0;

    private String imagePath1, imagePath2, imagePath3, videoPath4;

    private int state1 = 0, state2 = 0, state3 = 0, state4 = 0;

    private AlertDialog uploadAlertDialog = null;

    private AVObject reportObject;

    @Override
    public int layoutRes() {
        return R.layout.activity_device_report;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final int schoolIndex = AVUser.getCurrentUser().getInt("SchoolIndex");
        SchoolManager.getInstance().getSchoolList(new FindCallback<AVObject>() {
            @Override
            public void done(List<AVObject> list, AVException e) {
                if (e == null) {
                    for (AVObject object : list) {
                        if (object.getInt("Index") == schoolIndex) {
                            tv_school_name.setText(object.getString("Name"));
                            break;
                        }
                    }
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        KeyboardUtils.closeKeybord(activityThis);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_IMAGE:
                    if (data != null) {
                        Uri uri = data.getData();
                        if (uri != null) {
                            String path = RealPathFromUriUtils.getRealPathFromUri(this, data.getData());
                            Bitmap source = BitmapFactory.decodeFile(path);
                            Bitmap bitmap = ThumbnailUtils.extractThumbnail(source, 250, 250);
                            switch (index) {
                                case 1:
                                    imagePath1 = path;
                                    iv_media1.setImageBitmap(bitmap);
                                    break;
                                case 2:
                                    imagePath2 = path;
                                    iv_media2.setImageBitmap(bitmap);
                                    break;
                                case 3:
                                    imagePath3 = path;
                                    iv_media3.setImageBitmap(bitmap);
                                    break;
                                default:
                                    break;
                            }

                        }
                    }
                    break;
                case REQUEST_VIDEO:
                    if (data != null) {
                        Uri uri = data.getData();
                        if (uri != null) {
                            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
                            if (cursor != null) {
                                cursor.moveToFirst();
                                String path = cursor.getString(1);
                                videoPath4 = path;
                                Bitmap bitmap = ThumbnailUtils.createVideoThumbnail(path, MediaStore.Video.Thumbnails.MINI_KIND);
                                iv_media4.setVisibility(View.VISIBLE);
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                    iv_media4.setBackground(new BitmapDrawable(null, bitmap));
                                } else {
                                    iv_media4.setBackgroundDrawable(new BitmapDrawable(bitmap));
                                }
                                cursor.close();
                            } else {
                                showToast("找不到文件");
                            }
                        }
                    }
                    break;
                case REQUEST_BIND_DEVICE:
                    if (data.hasExtra("device")) {
                        device = (Device) data.getSerializableExtra("device");
                        tv_device_name.setText(String.format("%s(%s)", device.getDescription(), device.getName()));
                    } else {
                        device = null;
                        tv_device_name.setText("");
                    }
                    break;
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_PERMISSION_IMAGE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                selectImage();
            } else {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    new AlertDialog.Builder(activityThis)
                            .setMessage("获取读写权限，才能将图片信息上传到服务器")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    ActivityCompat.requestPermissions(activityThis,
                                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_PERMISSION_IMAGE
                                    );
                                }
                            })
                            .setNegativeButton("Cancel", null)
                            .create()
                            .show();
                } else {
                    showToast("请在权限管理中打开相关权限");
                }
            }
        } else if (requestCode == REQUEST_PERMISSION_VIDEO) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                selectVideo();
            } else {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    new AlertDialog.Builder(activityThis)
                            .setMessage("获取读写权限，才能将图片信息上传到服务器")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    ActivityCompat.requestPermissions(activityThis,
                                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_PERMISSION_IMAGE
                                    );
                                }
                            })
                            .setNegativeButton("Cancel", null)
                            .create()
                            .show();
                } else {
                    showToast("请在权限管理中打开相关权限");
                }
            }
        }
    }

    @OnClick(value = {R.id.rl_media1, R.id.rl_media2, R.id.rl_media3})
    protected void clickToAddImage(View view) {
        switch (view.getId()) {
            case R.id.rl_media1:
                index = 1;
                break;
            case R.id.rl_media2:
                index = 2;
                break;
            case R.id.rl_media3:
                index = 3;
                break;
        }
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
            int hasWriteStoragePermission = ContextCompat.checkSelfPermission(getApplication(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
            int hasReadStoragePermission = ContextCompat.checkSelfPermission(getApplication(), Manifest.permission.READ_EXTERNAL_STORAGE);
            if (hasWriteStoragePermission == PackageManager.PERMISSION_GRANTED && hasReadStoragePermission == PackageManager.PERMISSION_GRANTED) {
                //拥有权限，执行操作
                selectImage();
            } else {
                //没有权限，向用户请求权限
                ActivityCompat.requestPermissions(activityThis, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_PERMISSION_IMAGE);
            }
        } else {
            selectImage();
        }
    }

    @OnClick(R.id.rl_media4)
    protected void clickToAddVideo() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
            int hasWriteStoragePermission = ContextCompat.checkSelfPermission(getApplication(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
            int hasReadStoragePermission = ContextCompat.checkSelfPermission(getApplication(), Manifest.permission.READ_EXTERNAL_STORAGE);
            if (hasWriteStoragePermission == PackageManager.PERMISSION_GRANTED && hasReadStoragePermission == PackageManager.PERMISSION_GRANTED) {
                //拥有权限，执行操作
                selectVideo();
            } else {
                //没有权限，向用户请求权限
                ActivityCompat.requestPermissions(activityThis, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_PERMISSION_VIDEO);
            }
        } else {
            selectVideo();
        }
    }

    @OnClick(R.id.rl_report_device)
    protected void chooseDevice() {
        Intent intent = new Intent(activityThis, ReportDevicePopupActivity.class);
        if (device != null) {
            intent.putExtra("device", device);
        }
        startActivityForResult(intent, REQUEST_BIND_DEVICE);
        overridePendingTransition(0, 0);
    }

    ImageView iv_done = null;

    @OnClick(R.id.btn_report)
    protected void report() {
        if (et_report.getText().toString().length() > 0 && device != null) {
            reportObject = new AVObject("Report");
            reportObject.put("UserMobile", AVUser.getCurrentUser().getMobilePhoneNumber());
            reportObject.put("Message", et_report.getText().toString());
            reportObject.put("SchoolName", tv_school_name.getText());
            reportObject.put("DeviceName", tv_device_name.getText());
            try {
                if (!TextUtils.isEmpty(imagePath1)) {
                    File file = new File(imagePath1);
                    AVFile avfile = AVFile.withFile(file.getName(), file);
                    reportObject.put("Image1", avfile);
                    avfile.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(AVException e) {
                            state1 = e == null ? 1 : -1;
                            if (state2 != 0 && state3 != 0 && state4 != 0) {
                                uploadDone();
                            }
                        }
                    });
                } else {
                    state1 = 1;
                }
                if (!TextUtils.isEmpty(imagePath2)) {
                    File file = new File(imagePath2);
                    AVFile avfile = AVFile.withFile(file.getName(), file);
                    reportObject.put("Image2", avfile);
                    avfile.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(AVException e) {
                            state2 = e == null ? 1 : -1;
                            if (state1 != 0 && state3 != 0 && state4 != 0) {
                                uploadDone();
                            }
                        }
                    });
                } else {
                    state2 = 1;
                }
                if (!TextUtils.isEmpty(imagePath3)) {
                    File file = new File(imagePath3);
                    AVFile avfile = AVFile.withFile(file.getName(), file);
                    reportObject.put("Image3", avfile);
                    avfile.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(AVException e) {
                            state3 = e == null ? 1 : -1;
                            if (state1 != 0 && state2 != 0 && state4 != 0) {
                                uploadDone();
                            }
                        }
                    });
                } else {
                    state3 = 1;
                }
                if (!TextUtils.isEmpty(videoPath4)) {
                    File file = new File(videoPath4);
                    AVFile avfile = AVFile.withFile(file.getName(), file);
                    reportObject.put("Video", avfile);
                    avfile.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(AVException e) {
                            state4 = e == null ? 1 : -1;
                            if (state1 != 0 && state2 != 0 && state3 != 0) {
                                uploadDone();
                            }
                        }
                    });
                } else {
                    state4 = 1;
                }
                uploadAlertDialog = new AlertDialog.Builder(activityThis).setView(R.layout.view_alert_dialog_process)
                        .create();
                uploadAlertDialog.setCancelable(false);
                uploadAlertDialog.setCanceledOnTouchOutside(false);
                uploadAlertDialog.show();
                Window window = uploadAlertDialog.getWindow();
                assert window != null;
                window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));  // 有白色背景，加这句代码
                ((TextView) window.findViewById(R.id.tv_title)).setText("正在上传中");
                CircleBarView cbv_progress = window.findViewById(R.id.cbv_progress);
                TextView tv_percent = window.findViewById(R.id.tv_percent);
                iv_done = window.findViewById(R.id.iv_done);
                cbv_progress.setTextView(tv_percent);
                cbv_progress.setProgressNum(100, 10000);
                cbv_progress.setOnAnimationListener(new CircleBarView.OnAnimationListener() {
                    @Override
                    public String howToChangeText(float interpolatedTime, float updateNum, float maxNum) {
                        if (interpolatedTime < 1)
                            return (int) (interpolatedTime * updateNum / maxNum * 100) + "%";
                        else {
                            if (state1 != 0 && state2 != 0 && state3 != 0 && state4 != 0) {
                                uploadDone();
                            }
                            return "";
                        }
                    }

                    @Override
                    public void howTiChangeProgressColor(Paint paint, float interpolatedTime, float updateNum, float maxNum) {

                    }
                });
            } catch (Exception e) {
                showToast(e.getMessage());
            }
        } else if (device != null) {
            showToast("请写故障描述");
        } else {
            showToast("请先选择设备");
        }
    }

    private void selectImage() {
        KeyboardUtils.closeKeybord(et_report, activityThis);
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(intent, REQUEST_IMAGE);
    }

    private void selectVideo() {
        KeyboardUtils.closeKeybord(et_report, activityThis);
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("video/*");
        startActivityForResult(intent, REQUEST_VIDEO);
    }

    private void uploadDone() {
        reportObject.saveInBackground(new SaveCallback() {
            @Override
            public void done(AVException e) {
                if (e == null) {
                    if (iv_done != null) {
                        iv_done.setVisibility(View.VISIBLE);
                        iv_done.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                uploadAlertDialog.dismiss();
                                startActivity(DeviceReportDoneActivity.class);
                                finish();
                            }
                        }, 200);
                    }
                }
            }
        });
    }
}
