package cn.zxd.edu.ui;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.view.View;
import android.widget.RelativeLayout;

import com.alibaba.fastjson.JSON;
import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVFile;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.GetCallback;
import com.avos.avoscloud.GetDataCallback;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import butterknife.BindView;
import butterknife.OnClick;
import cn.zxd.edu.R;
import cn.zxd.edu.bean.Device;
import cn.zxd.edu.bean.StatusResponse;
import cn.zxd.edu.manager.DeviceManager;
import cn.zxd.edu.util.AVUtils;
import cn.zxd.edu.util.SharedPreferencesHelper;
import cn.zxd.edu.util.SuperHttpUtil;
import cn.zxd.edu.view.TitleView;
import cz.msebera.android.httpclient.Header;

public class DeviceControlActivity extends BaseActivity {

    @BindView(R.id.tv_device_control)
    TitleView tv_device_control;

    @BindView(R.id.rl_power_off)
    RelativeLayout rl_power_off;

    @BindView(R.id.cl_power_on)
    ConstraintLayout cl_power_on;

    ProgressDialog progressDialog;

    Device device = null;

    SharedPreferencesHelper helper;

    @Override
    public int layoutRes() {
        return R.layout.activity_device_control;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        helper = new SharedPreferencesHelper(activityThis, "Device_Control");
        device = (Device) getIntent().getSerializableExtra("device");
        if (AVUtils.isAdministrator(AVUser.getCurrentUser()) || AVUtils.isSuperAdministrator(AVUser.getCurrentUser())) {
            tv_device_control.setRightText("设备控制");
        } else {
            tv_device_control.setRightText("");
        }
        tv_device_control.setOnRightClickListener(new TitleView.OnRightClickListener() {
            @Override
            public void onRightClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putSerializable("device", device);
                startActivity(AuthorizeActivity.class, bundle);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
        progressDialog = new ProgressDialog(activityThis);
        progressDialog.setMessage("正在连接设备");
        progressDialog.show();
        SuperHttpUtil.post("/api/v1/status", new RequestParams("device", device.getName()), new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                progressDialog.dismiss();
                showToast(throwable.getMessage());
                finish();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                progressDialog.dismiss();
                StatusResponse response = JSON.parseObject(responseString, StatusResponse.class);
                if (response.status) {
                    rl_power_off.setVisibility(View.GONE);
                    cl_power_on.setVisibility(View.VISIBLE);
                } else {
                    rl_power_off.setVisibility(View.VISIBLE);
                    cl_power_on.setVisibility(View.GONE);
                }
            }
        });
    }

    @OnClick(R.id.iv_power_on)
    protected void shouldPowerOn() {
        long lastOffTime = (long) helper.getSharedPreference(device.getDeviceId() + "-off", 0L);
        if (System.currentTimeMillis() - lastOffTime <= 90000) {
            showToast("距上次关机时间太短，您还需要耐心" + (lastOffTime + 90000 - System.currentTimeMillis()) / 1000 + "秒");
        } else {
            new AlertDialog.Builder(activityThis).setTitle(device.getDescription())
                    .setMessage("请确认开机")
                    .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            powerOn();
                        }
                    })
                    .setNegativeButton("取消", null).create().show();
        }
    }


    protected void powerOn() {
        SuperHttpUtil.post("/api/v1/device/control", new RequestParams("device", device.getName(), "index", "8"), new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                showToast("开机失败，请检查硬件");
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                helper.put(device.getDeviceId() + "-on", System.currentTimeMillis());
                rl_power_off.setVisibility(View.GONE);
                cl_power_on.setVisibility(View.VISIBLE);
            }
        });
    }

    @OnClick(R.id.iv_power_off)
    protected void shouldPowerOff() {
        long lastOnTime = (long) helper.getSharedPreference(device.getDeviceId() + "-on", 0L);
        if (System.currentTimeMillis() - lastOnTime <= 90000) {
            showToast("距上次开机时间太短，您还需要耐心等待" + (lastOnTime + 90000 - System.currentTimeMillis()) / 1000 + "秒");
        } else {
            new AlertDialog.Builder(activityThis).setTitle(device.getDescription())
                    .setMessage("请确认关机")
                    .setPositiveButton("关机", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            powerOff();
                        }
                    })
                    .setNegativeButton("取消", null).create().show();
        }
    }


    protected void powerOff() {

        SuperHttpUtil.post("/api/v1/device/control", new RequestParams("device", device.getName(), "index", "8"), new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                showToast("关机失败");
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                helper.put(device.getDeviceId() + "-off", System.currentTimeMillis());
                rl_power_off.setVisibility(View.VISIBLE);
                cl_power_on.setVisibility(View.GONE);
            }
        });
    }

    @OnClick(R.id.btn_start_computer)
    protected void startComputer() {
        SuperHttpUtil.post("/api/v1/device/control", new RequestParams("device", device.getName(), "index", "11"), new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                showToast("操作失败");
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                showToast("操作成功");
            }
        });
    }

    @OnClick(R.id.btn_start_projector)
    protected void startProjector() {
        SuperHttpUtil.post("/api/v1/device/control", new RequestParams("device", device.getName(), "index", "10"), new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                showToast("操作失败");
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                showToast("操作成功");
            }
        });
    }

    @OnClick(R.id.btn_mute)
    protected void mute() {
        SuperHttpUtil.post("/api/v1/device/control", new RequestParams("device", device.getName(), "index", "5"), new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                showToast("操作失败");
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                showToast("操作成功");
            }
        });
    }

    @OnClick(R.id.btn_switch_projector)
    protected void switchProjector() {
        SuperHttpUtil.post("/api/v1/device/control", new RequestParams("device", device.getName(), "index", "6"), new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                showToast("操作失败");
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                showToast("操作成功");
            }
        });
    }

    @OnClick(R.id.btn_volume_up)
    protected void volumeUp() {
        SuperHttpUtil.post("/api/v1/device/control", new RequestParams("device", device.getName(), "index", "9"), new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                showToast("操作失败");
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                showToast("操作成功");
            }
        });
    }

    @OnClick(R.id.btn_volume_down)
    protected void volumeDown() {
        SuperHttpUtil.post("/api/v1/device/control", new RequestParams("device", device.getName(), "index", "7"), new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                showToast("操作失败");
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                showToast("操作成功");
            }
        });
    }

    @OnClick({R.id.iv_camera, R.id.iv_camera_small})
    protected void toCamera() {
        showToast("跳转到网络摄像头");
    }

    @OnClick({R.id.tv_device_info, R.id.tv_device_info_small})
    protected void toInfo() {
        Device device = (Device) getIntent().getSerializableExtra("device");
        DeviceManager.getInstance().getDeviceInfo(device, new GetCallback<AVObject>() {
            @Override
            public void done(AVObject avObject, AVException e) {
                AVFile file = avObject.getAVFile("Information");
                if (file != null) {
                    Bundle bundle = new Bundle();
                    bundle.putString("url", file.getUrl());
                    startActivity(DeviceInformationActivity.class, bundle);
                } else {
                    showToast("暂无设备信息");
                }
            }
        });
    }

}
