package cn.zxd.edu.ui;

public abstract class PopupActivity extends BaseActivity {

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(0,0);
    }
}
