package cn.zxd.edu.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.zxd.edu.R;
import cn.zxd.edu.adapter.CommonAdapter;
import cn.zxd.edu.adapter.CommonViewHolder;
import cn.zxd.edu.adapter.ImageViewAdapter;
import cn.zxd.edu.bean.NewsListItem;
import cn.zxd.edu.bean.ProductGridItem;
import cn.zxd.edu.view.CircleIndicator;
import cn.zxd.edu.view.GridViewForScrollView;

public class FindFragment extends BaseFragment {

    @BindView(R.id.vp_banner)
    ViewPager vp_banner;

    @BindView(R.id.ci_dots)
    CircleIndicator ci_dots;

    @BindView(R.id.gv_product)
    GridViewForScrollView gv_product;

    @BindView(R.id.lv_news)
    ListView lv_news;

    private List<ProductGridItem> products = new ArrayList<>();

    private List<NewsListItem> news = new ArrayList<>();

    @Override
    public int layoutRes() {
        return R.layout.fragment_find;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        vp_banner.setAdapter(new ImageViewAdapter());
        ci_dots.setViewPager(vp_banner);
        initData();
        gv_product.setAdapter(new CommonAdapter<ProductGridItem>(getActivity(), products, R.layout.view_product_grid) {
            @Override
            public void convert(CommonViewHolder helper, ProductGridItem item) {
                helper.setImageResource(R.id.iv_product_image, item.getDrawableRes());
                helper.setText(R.id.tv_product_name, item.getName());
            }
        });
        gv_product.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ProductGridItem item = (ProductGridItem) parent.getItemAtPosition(position);
                Bundle bundle = new Bundle();
                bundle.putString("url", item.getUrl());
                startActivity(Html5Activity.class, bundle);
            }
        });
        lv_news.setAdapter(new CommonAdapter<NewsListItem>(getActivity(), news, R.layout.view_news_list) {
            @Override
            public void convert(CommonViewHolder helper, NewsListItem item) {
                helper.setText(R.id.tv_news_title, item.getTitle());
            }
        });
        lv_news.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                NewsListItem item = (NewsListItem) parent.getItemAtPosition(position);
                Bundle bundle = new Bundle();
                bundle.putString("url", item.getUrl());
                startActivity(Html5Activity.class, bundle);
            }
        });
    }

    @OnClick(R.id.tv_more_product)
    protected void viewMoreProduct() {
        Bundle bundle = new Bundle();
        bundle.putString("url", "http://www.szigy.com/Product/");
        startActivity(Html5Activity.class, bundle);
    }

    @OnClick(R.id.tv_more_news)
    protected void viewMoreNews() {
        Bundle bundle = new Bundle();
        bundle.putString("url", "http://www.szigy.com/News/CompanyNews/");
        startActivity(Html5Activity.class, bundle);
    }

    private void initData() {
        products.add(new ProductGridItem("GY-DMT019", R.drawable.product1, "http://www.szigy.com/Product/617903314.html"));
        products.add(new ProductGridItem("GY-DMT005", R.drawable.product2, "http://www.szigy.com/Product/3758401158.html"));
        products.add(new ProductGridItem("GY-DMT029", R.drawable.product3, "http://www.szigy.com/Product/9564211948.html"));
        products.add(new ProductGridItem("GY-DMT003", R.drawable.product4, "http://www.szigy.com/Product/501249201.html"));
        products.add(new ProductGridItem("GY-DMT018", R.drawable.product5, "http://www.szigy.com/Product/503482448.html"));
        products.add(new ProductGridItem("GY-DMT002", R.drawable.product6, "http://www.szigy.com/Product/5406824215.html"));
        news.add(new NewsListItem("耕耘教备为教学服务机构保驾护航-记云顶精致教育炼成记", "http://www.szigy.com/html/6921784321.html"));
        news.add(new NewsListItem("第68届中国教育装备展，我们来了", "http://www.szigy.com/html/6897412447.html"));
        news.add(new NewsListItem("邀请函——诚挚邀您莅临第68届中国教育装备展进行参观、指导、交流、合作！", "http://www.szigy.com/html/803264534.html"));
        news.add(new NewsListItem("公司参加“慧聪网”“跨界、融合；远见、未来”集成商大会", "http://www.szigy.com/html/3654013947.html"));
        news.add(new NewsListItem("公司参加第66届中国（昆明）教育装备展", "http://www.szigy.com/html/9630152541.html"));
    }
}
