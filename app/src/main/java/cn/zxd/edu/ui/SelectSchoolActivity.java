package cn.zxd.edu.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.TextView;

import butterknife.BindView;
import cn.zxd.edu.R;

public class SelectSchoolActivity extends ChooseSchoolActivity {

    @BindView(R.id.tv_choose_school_detail)
    TextView tv_choose_school_detail;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tv_choose_school_detail.setText("请选择更换的学校");
    }
}
