package cn.zxd.edu.ui;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.MenuItem;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.FindCallback;

import java.util.List;

import butterknife.BindView;
import cn.zxd.edu.R;
import cn.zxd.edu.adapter.FragmentAdapter;
import cn.zxd.edu.manager.SchoolManager;
import cn.zxd.edu.util.SuperHttpUtil;

public class MainActivity extends BaseActivity {

    @BindView(R.id.bnv_navigation)
    BottomNavigationView bnv_navigation;

    @BindView(R.id.vp_container)
    ViewPager vp_container;

    BottomNavigationView.OnNavigationItemSelectedListener navigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
            switch (menuItem.getItemId()) {
                case R.id.navigation_device:
                    vp_container.setCurrentItem(0);
                    return true;
                case R.id.navigation_find:
                    vp_container.setCurrentItem(1);
                    return true;
                case R.id.navigation_me:
                    vp_container.setCurrentItem(2);
                    return true;
                default:
                    return false;
            }
        }
    };

    ViewPager.OnPageChangeListener viewPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int i, float v, int i1) {

        }

        @Override
        public void onPageSelected(int i) {
            switch (i) {
                case 1:
                    bnv_navigation.setSelectedItemId(R.id.navigation_find);
                    break;
                case 2:
                    bnv_navigation.setSelectedItemId(R.id.navigation_me);
                    break;
                case 0:
                default:
                    bnv_navigation.setSelectedItemId(R.id.navigation_device);
                    break;
            }
        }

        @Override
        public void onPageScrollStateChanged(int i) {

        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bnv_navigation.setOnNavigationItemSelectedListener(navigationItemSelectedListener);
        bnv_navigation.setSelectedItemId(R.id.navigation_device);
        vp_container.setAdapter(new FragmentAdapter(getSupportFragmentManager()));
        vp_container.addOnPageChangeListener(viewPageChangeListener);
        vp_container.setCurrentItem(0);
    }

    @Override
    public int layoutRes() {
        return R.layout.activity_main;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (TextUtils.isEmpty(SuperHttpUtil.HttpServerIp)) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (AVUser.getCurrentUser() != null) {
                            AVUser.getCurrentUser().fetch();
                            if (AVUser.getCurrentUser().getInt("SchoolIndex") > 0) {
                                SchoolManager.getInstance().getSchoolList(new FindCallback<AVObject>() {
                                    @Override
                                    public void done(List<AVObject> list, AVException e) {
                                        for (AVObject object : list) {
                                            if (object.getInt("Index") == AVUser.getCurrentUser().getInt("SchoolIndex")) {
                                                SuperHttpUtil.HttpServerIp = "http://" + object.getString("ServerIP");
                                            }
                                        }
                                    }
                                });
                            }
                        }
                    } catch (AVException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }
    }
}
