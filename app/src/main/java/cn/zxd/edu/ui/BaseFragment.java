package cn.zxd.edu.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.avos.avoscloud.AVUser;

import butterknife.ButterKnife;
import cn.zxd.edu.AppApplication;
import cn.zxd.edu.util.DeviceUtils;

public abstract class BaseFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(layoutRes(), null);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
            view.setPadding(0, DeviceUtils.getStatusBarHeight(AppApplication.getAppContext()), 0, 0);
        }
        ButterKnife.bind(this, view);
        return view;
    }

    public void startActivity(Class<? extends Activity> clazz) {
        startActivity(new Intent(getContext(), clazz));
    }

    public void startActivity(Class<? extends Activity> clazz, Bundle bundle) {
        startActivity(new Intent(getContext(), clazz).putExtras(bundle));
    }

    protected void showToast(CharSequence text) {
        Toast.makeText(getActivity(), text, Toast.LENGTH_SHORT).show();
    }

    public abstract int layoutRes();
}
