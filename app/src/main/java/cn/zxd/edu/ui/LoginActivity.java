package cn.zxd.edu.ui;

import android.os.CountDownTimer;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.TextView;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVSMS;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.LogInCallback;
import com.avos.avoscloud.RequestMobileCodeCallback;

import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;
import cn.zxd.edu.R;

public class LoginActivity extends BaseActivity {

    @BindView(R.id.et_mobile)
    EditText et_mobile;

    @BindView(R.id.et_sms)
    EditText et_sms;

    @BindView(R.id.tv_sms)
    TextView tv_sms;

    @Override
    public int layoutRes() {
        return R.layout.activity_login;
    }

    boolean hasSms = false;
    String mobile;

    @OnClick(R.id.tv_sms)
    protected void getSms() {
        mobile = et_mobile.getText().toString();
        if (mobile.matches("^(\\d){11}$")) {
            AVSMS.requestSMSCodeInBackground(mobile, null, new RequestMobileCodeCallback() {
                @Override
                public void done(AVException e) {
                    if (e == null) {
                        hasSms = true;
                        showToast("短信已发送");
                        tv_sms.setClickable(false);
                        new CountDownTimer(60000, 1000) {

                            @Override
                            public void onTick(long millisUntilFinished) {
                                tv_sms.setText(String.format(Locale.getDefault(), "重新获取(%ds)", millisUntilFinished / 1000));
                            }

                            @Override
                            public void onFinish() {
                                tv_sms.setText("重新获取");
                                tv_sms.setClickable(true);
                            }
                        }.start();
                    } else {
                        showToast(e.getMessage());
                    }
                }
            });
        } else {
            showToast("请输入正确的手机号");
        }
    }

    @OnClick(R.id.btn_login)
    protected void login() {
//        if (!hasSms) {
//            showToast("请先获取短信验证码");
//        } else {
        mobile = et_mobile.getText().toString();
        String sms = et_sms.getText().toString();
        if (sms.matches("^(\\d){6}$")) {
            AVUser.signUpOrLoginByMobilePhoneInBackground(mobile, sms, new LogInCallback<AVUser>() {
                @Override
                public void done(AVUser user, AVException e) {
                    if (e == null) {
                        if (TextUtils.isEmpty(user.getString("nickname"))) {
                            startActivity(NickNameActivity.class);
                        } else if (user.getInt("SchoolIndex") <= 0) {
                            startActivity(ChooseSchoolActivity.class);
                        }
                        finish();
                    } else {
                        showToast(e.getMessage());
                    }
                }
            });
        } else {
            showToast("请输入正确的验证码");
        }
//        }
    }

    @OnClick(R.id.iv_close)
    protected void close() {
        finish();
    }

}
