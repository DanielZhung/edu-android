package cn.zxd.edu.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.OnClick;
import cn.zxd.edu.R;
import cn.zxd.edu.bean.Device;
import cn.zxd.edu.manager.DeviceManager;
import cn.zxd.edu.view.CircleBarView;

public class AddDeviceActivity extends BaseActivity {

    private static final int REQUEST_UNBIND_DEVICE = 102;

    @BindView(R.id.tv_device_name)
    TextView tv_device_name;

    @BindView(R.id.et_device_detail)
    EditText et_device_detail;

    private Device device = null;

    private AlertDialog addDeviceAlertDialog = null;

    @Override
    public int layoutRes() {
        return R.layout.activity_add_device;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_UNBIND_DEVICE && resultCode == RESULT_OK) {
            device = (Device) data.getSerializableExtra("device");
            if (device != null) {
                tv_device_name.setText(device.getName());
                tv_device_name.setTextColor(0xFF333333);
            } else {
                tv_device_name.setText("请选择");
                tv_device_name.setTextColor(0xFF777777);
            }
        }
    }

    @OnClick(R.id.rl_choose_device)
    protected void showOnlineDevices(View view) {
        Intent intent = new Intent(activityThis, SelectDevicePopupActivity.class);
        if (device != null) {
            intent.putExtra("device", device);
        }
        startActivityForResult(intent, REQUEST_UNBIND_DEVICE);
        overridePendingTransition(0, 0);
    }

    @OnClick(R.id.btn_add_device)
    protected void addDevice() {
        if (device == null) {
            showToast("请先选择要添加的设备");
        } else if (TextUtils.isEmpty(et_device_detail.getText().toString())) {
            new AlertDialog.Builder(activityThis).setTitle("提示").setMessage("添加设备描述能更好的帮助您定位设备位置,请填写设备描述")
                    .setPositiveButton("确定", null).create().show();
        } else {
            addDevice(device, et_device_detail.getText().toString());
        }
    }

    private void addDevice(Device device, String description) {
        DeviceManager.getInstance().addDevice(device, description);
        addDeviceAlertDialog = new AlertDialog.Builder(activityThis).setView(R.layout.view_alert_dialog_process)
                .create();
        addDeviceAlertDialog.setCancelable(false);
        addDeviceAlertDialog.setCanceledOnTouchOutside(false);
        addDeviceAlertDialog.show();
        Window window = addDeviceAlertDialog.getWindow();
        assert window != null;
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));  // 有白色背景，加这句代码
        ((TextView) window.findViewById(R.id.tv_title)).setText("设备正在连接");
        CircleBarView cbv_progress = window.findViewById(R.id.cbv_progress);
        TextView tv_percent = window.findViewById(R.id.tv_percent);
        final ImageView iv_done = window.findViewById(R.id.iv_done);
        cbv_progress.setTextView(tv_percent);
        cbv_progress.setProgressNum(100, 3000);
        cbv_progress.setOnAnimationListener(new CircleBarView.OnAnimationListener() {
            @Override
            public String howToChangeText(float interpolatedTime, float updateNum, float maxNum) {
                if (interpolatedTime < 1)
                    return (int) (interpolatedTime * updateNum / maxNum * 100) + "%";
                else {
                    iv_done.setVisibility(View.VISIBLE);
                    iv_done.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    addDone();
                                }
                            });
                        }
                    }, 300);
                    return "";
                }
            }

            @Override
            public void howTiChangeProgressColor(Paint paint, float interpolatedTime, float updateNum, float maxNum) {

            }
        });
    }

    private void addDone() {
        addDeviceAlertDialog.dismiss();
        finish();
    }
}
