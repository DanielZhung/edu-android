package cn.zxd.edu.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.FindCallback;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnItemClick;
import cn.zxd.edu.R;
import cn.zxd.edu.adapter.CommonAdapter;
import cn.zxd.edu.adapter.CommonViewHolder;
import cn.zxd.edu.bean.Device;
import cn.zxd.edu.manager.DeviceManager;
import cn.zxd.edu.util.ScreenUtils;

public class SelectDevicePopupActivity extends PopupActivity implements Animation.AnimationListener {

    @BindView(R.id.cl_content)
    ConstraintLayout cl_content;

    @BindView(R.id.lv_unbind_devices)
    ListView lv_unbind_devices;

    protected List<Device> unbindDevices = new ArrayList<>();

    protected Device selectDevice;

    @Override
    public int layoutRes() {
        return R.layout.activity_popup_select_devices;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        enterAnimation();
        selectDevice = (Device) getIntent().getSerializableExtra("device");
        initAdapter();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getDevice();
    }

    protected void getDevice() {
        DeviceManager.getInstance().getUnbindDeviceList(new FindCallback<AVObject>() {
            @Override
            public void done(List<AVObject> list, AVException e) {
                if (e == null) {
                    unbindDevices.clear();
                    for (AVObject object : list) {
                        Device device = new Device(object.getString("Name"), object.getObjectId(), object.getString("Description"), object.getBoolean("Online"), object.getBoolean("On"), object.getInt("SchoolIndex"));
                        if (selectDevice != null && device.getName().equals(selectDevice.getName())) {
                            device.setSelected(true);
                        }
                        unbindDevices.add(device);
                    }
                    ((BaseAdapter) lv_unbind_devices.getAdapter()).notifyDataSetChanged();
                } else {
                    showToast(e.getMessage());
                }
            }
        }, AVUser.getCurrentUser().getInt("SchoolIndex"), true);
    }

    @Override
    public void onBackPressed() {
        tryFinish();
    }

    @OnClick(R.id.cl_root)
    protected void dismiss() {
        onBackPressed();
    }

    @OnItemClick(R.id.lv_unbind_devices)
    protected void selectDevices(AdapterView<?> parent, View view, int position, long id) {
        for (int i = 0; i < unbindDevices.size(); i++) {
            Device device = unbindDevices.get(i);
            if (i == position) {
                device.setSelected(!device.isSelected());
            } else {
                device.setSelected(false);
            }
        }
        ((BaseAdapter) lv_unbind_devices.getAdapter()).notifyDataSetChanged();
        Intent data = new Intent();
        Device device = unbindDevices.get(position);
        if (device.isSelected()) {
            data.putExtra("device", device);
        }
        setResult(RESULT_OK, data);
        tryFinish();
    }

    private void tryFinish() {
        exitAnimation();
    }

    private void enterAnimation() {
        float screenHeight = ScreenUtils.getScreenHeight(activityThis);
        Animation animation = new TranslateAnimation(0, 0, screenHeight / 2, 0);
        animation.setDuration(300);
        animation.setFillAfter(true);
        cl_content.startAnimation(animation);
    }

    private void exitAnimation() {
        float screenHeight = ScreenUtils.getScreenHeight(activityThis);
        Animation animation = new TranslateAnimation(0, 0, 0, screenHeight / 2);
        animation.setDuration(300);
        animation.setFillAfter(true);
        animation.setAnimationListener(this);
        cl_content.startAnimation(animation);
    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {
        super.onBackPressed();
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }

    protected void initAdapter() {
        lv_unbind_devices.setAdapter(new CommonAdapter<Device>(activityThis, unbindDevices, R.layout.view_unbind_device_list) {
            @Override
            public void convert(CommonViewHolder helper, Device item) {
                helper.setText(R.id.tv_unbind_device, item.getName());
                helper.getView(R.id.iv_device_select).setVisibility(item.isSelected() ? View.VISIBLE : View.GONE);
            }
        });
    }
}
