package cn.zxd.edu.ui;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.FindCallback;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.zxd.edu.R;
import cn.zxd.edu.manager.SchoolManager;
import cn.zxd.edu.util.AVUtils;
import cn.zxd.edu.view.SelectorView;

public class MeFragment extends BaseFragment {

    @BindView(R.id.tv_login)
    TextView tv_login;

    @BindView(R.id.tv_me_name)
    TextView tv_me_name;

    @BindView(R.id.iv_avatar)
    ImageView iv_avatar;

    @BindView(R.id.ll_me_id)
    LinearLayout ll_me_id;

    @BindView(R.id.iv_id)
    ImageView iv_id;

    @BindView(R.id.tv_id)
    TextView tv_id;

    @BindView(R.id.tv_school)
    TextView tv_school;

    @BindView(R.id.sv_3)
    SelectorView sv_3;

    @Override
    public int layoutRes() {
        return R.layout.fragment_me;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        initAvatar();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (iv_avatar != null) {
            initAvatar();
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && iv_avatar != null) {
            initAvatar();
        }
    }

    @OnClick(R.id.tv_login)
    protected void toLogin() {
        startActivity(LoginActivity.class);
    }

    @OnClick(R.id.sv_1)
    protected void toManual() {
        startActivity(ManualActivity.class);
    }

    @OnClick(R.id.sv_2)
    protected void toFeedback() {
        startActivity(FeedbackActivity.class);
    }

    @OnClick(R.id.sv_3)
    protected void toDeviceReport() {
        startActivity(DeviceReportActivity.class);
    }

    @OnClick(R.id.sv_4)
    protected void toSettings() {
        startActivity(SettingsActivity.class);
    }

    @OnClick(R.id.sv_5)
    protected void toAboutUs() {
        startActivity(AboutUsActivity.class);
    }

    @OnClick(R.id.tv_tel)
    protected void toCall() {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        Uri data = Uri.parse("tel:4000014415");
        intent.setData(data);
        startActivity(intent);
    }

    @OnClick(R.id.iv_we_chat)
    protected void toWeChat() {
        showToast("跳转微信公众号,开发中");
    }

    @OnClick(R.id.tv_school)
    protected void changeSchool() {
        if (AVUtils.isSuperAdministrator(AVUser.getCurrentUser())) {
            startActivity(SelectSchoolActivity.class);
        }
    }

    private void initAvatar() {
        AVUser user = AVUser.getCurrentUser();
        if (user == null) {
            tv_login.setVisibility(View.VISIBLE);
            iv_avatar.setImageResource(R.drawable.known_avatar);
            ll_me_id.setVisibility(View.GONE);
            tv_me_name.setText("您尚未登录");
            sv_3.setVisibility(View.GONE);
            tv_school.setVisibility(View.GONE);
        } else if (AVUtils.isSuperAdministrator(user)) {
            tv_login.setVisibility(View.GONE);
            ll_me_id.setVisibility(View.VISIBLE);
            tv_school.setVisibility(View.VISIBLE);
            iv_avatar.setImageResource(R.drawable.admin_avatar);
            tv_me_name.setText(user.getString("nickname"));
            iv_id.setImageResource(R.drawable.administrator);
            tv_id.setText("超级管理员");
            sv_3.setVisibility(View.VISIBLE);
            initSchool(tv_school, user.getInt("SchoolIndex"));
        } else if (AVUtils.isAdministrator(user)) {
            tv_login.setVisibility(View.GONE);
            ll_me_id.setVisibility(View.VISIBLE);
            tv_school.setVisibility(View.VISIBLE);
            iv_avatar.setImageResource(R.drawable.admin_avatar);
            tv_me_name.setText(user.getString("nickname"));
            iv_id.setImageResource(R.drawable.administrator);
            tv_id.setText("学校管理员");
            sv_3.setVisibility(View.VISIBLE);
            initSchool(tv_school, user.getInt("SchoolIndex"));
        } else {
            tv_login.setVisibility(View.GONE);
            ll_me_id.setVisibility(View.VISIBLE);
            tv_school.setVisibility(View.VISIBLE);
            iv_avatar.setImageResource(R.drawable.normal_avatar);
            tv_me_name.setText(user.getString("nickname"));
            iv_id.setImageResource(R.drawable.normal_teacher);
            tv_id.setText("普通教师");
            sv_3.setVisibility(View.GONE);
            initSchool(tv_school, user.getInt("SchoolIndex"));
        }
    }

    private void initSchool(final TextView textView, final int index) {
        SchoolManager.getInstance().getSchoolList(new FindCallback<AVObject>() {
            @Override
            public void done(List<AVObject> list, AVException e) {
                if (e == null) {
                    for (AVObject object : list) {
                        if (object.getInt("Index") == index) {
                            textView.setText(object.getString("Name"));
                            break;
                        }
                    }
                }
            }
        });
    }

}
