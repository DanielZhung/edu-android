package cn.zxd.edu.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.FindCallback;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnItemClick;
import cn.zxd.edu.R;
import cn.zxd.edu.adapter.CommonAdapter;
import cn.zxd.edu.adapter.CommonViewHolder;
import cn.zxd.edu.bean.School;
import cn.zxd.edu.manager.SchoolManager;

public class ChooseSchoolActivity extends BaseActivity {

    @BindView(R.id.lv_schools)
    ListView lv_schools;

    List<School> schools = new ArrayList<>();

    @Override
    public int layoutRes() {
        return R.layout.activity_choose_school;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        lv_schools.setAdapter(new CommonAdapter<School>(activityThis, schools, R.layout.text_view_school_list) {
            @Override
            public void convert(CommonViewHolder helper, School item) {
                helper.setText(R.id.tv_school_name, item.getName());
            }
        });
        SchoolManager.getInstance().getSchoolList(new FindCallback<AVObject>() {
            @Override
            public void done(List<AVObject> list, AVException e) {
                if (e == null) {
                    for (AVObject object : list) {
                        schools.add(new School(object.getInt("Index"), object.getString("Name")));
                    }
                    ((BaseAdapter) lv_schools.getAdapter()).notifyDataSetChanged();
                }
            }
        });
    }

    @OnItemClick(R.id.lv_schools)
    protected void chooseSchool(AdapterView<?> parent, View view, int position, long id) {
        AVUser user = AVUser.getCurrentUser();
        user.put("SchoolIndex", ((School) parent.getAdapter().getItem(position)).getIndex());
        user.saveInBackground();
        finish();
    }

    @OnClick(R.id.iv_close)
    protected void close() {
        finish();
    }
}
