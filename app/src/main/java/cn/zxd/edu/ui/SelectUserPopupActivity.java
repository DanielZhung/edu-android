package cn.zxd.edu.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.FindCallback;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnItemClick;
import cn.zxd.edu.R;
import cn.zxd.edu.adapter.CommonAdapter;
import cn.zxd.edu.adapter.CommonViewHolder;
import cn.zxd.edu.bean.Device;
import cn.zxd.edu.bean.User;
import cn.zxd.edu.manager.UserManager;
import cn.zxd.edu.util.AVUtils;
import cn.zxd.edu.util.ScreenUtils;

public class SelectUserPopupActivity extends PopupActivity implements Animation.AnimationListener {

    @BindView(R.id.cl_content)
    ConstraintLayout cl_content;

    @BindView(R.id.lv_unauthorized_user)
    ListView lv_unauthorized_user;

    Device device = null;

    List<User> unauthorizedUsers = new ArrayList<>();

    @Override
    public int layoutRes() {
        return R.layout.activity_popup_select_user;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        enterAnimation();
        device = (Device) getIntent().getSerializableExtra("device");
        lv_unauthorized_user.setAdapter(new CommonAdapter<User>(activityThis, unauthorizedUsers, R.layout.view_unbind_device_list) {
            @Override
            public void convert(CommonViewHolder helper, User item) {
                helper.setText(R.id.tv_unbind_device, item.getNickname() + "(" + item.getMobile() + ")");
                helper.getView(R.id.iv_device_select).setVisibility(item.isChecked() ? View.VISIBLE : View.GONE);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        UserManager.getInstance().getUnauthorizedUser(device.getSchoolIndex(), new FindCallback<AVUser>() {
            @Override
            public void done(List<AVUser> list, AVException e) {
                if (e == null) {
                    unauthorizedUsers.clear();
                    for (AVUser user : list) {
                        if (!AVUtils.isAdministrator(user) && !AVUtils.isSuperAdministrator(user) && UserManager.getInstance().notAuthorized(user))
                            unauthorizedUsers.add(new User(user.getMobilePhoneNumber(), user.getString("nickname")));
                    }
                    ((BaseAdapter) lv_unauthorized_user.getAdapter()).notifyDataSetChanged();
                } else {
                    showToast(e.getMessage());
                }
            }
        });
    }

    @OnClick(R.id.cl_root)
    protected void dismiss() {
        onBackPressed();
    }

    @OnClick(R.id.tv_done)
    protected void authorizedUser() {
        if (hasSelectUser(unauthorizedUsers)) {
            Intent data = new Intent();
            data.putExtra("users", (Serializable) unauthorizedUsers);
            this.setResult(RESULT_OK, data);
        } else {
            this.setResult(RESULT_CANCELED);
        }
        finish();
    }

    @OnItemClick(R.id.lv_unauthorized_user)
    protected void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        User user = unauthorizedUsers.get(position);
        user.setChecked(!user.isChecked());
        ((BaseAdapter) lv_unauthorized_user.getAdapter()).notifyDataSetChanged();
    }

    @Override
    public void onBackPressed() {
        exitAnimation();
    }

    private void enterAnimation() {
        float screenHeight = ScreenUtils.getScreenHeight(activityThis);
        Animation animation = new TranslateAnimation(0, 0, screenHeight / 2, 0);
        animation.setDuration(300);
        animation.setFillAfter(true);
        cl_content.startAnimation(animation);
    }

    private void exitAnimation() {
        float screenHeight = ScreenUtils.getScreenHeight(activityThis);
        Animation animation = new TranslateAnimation(0, 0, 0, screenHeight / 2);
        animation.setDuration(300);
        animation.setFillAfter(true);
        animation.setAnimationListener(this);
        cl_content.startAnimation(animation);
    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {
        super.onBackPressed();
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }

    private boolean hasSelectUser(List<User> users) {
        for (User user : users) {
            if (user.isChecked()) {
                return true;
            }
        }
        return false;
    }
}
