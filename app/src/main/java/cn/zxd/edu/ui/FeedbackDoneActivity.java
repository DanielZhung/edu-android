package cn.zxd.edu.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import cn.zxd.edu.R;
import cn.zxd.edu.view.TitleView;

public class FeedbackDoneActivity extends BaseActivity {

    @BindView(R.id.tv_done)
    TitleView tv_done;

    @BindView(R.id.iv_image)
    ImageView iv_image;

    @BindView(R.id.tv_info)
    TextView tv_info;

    @BindView(R.id.tv_detail)
    TextView tv_detail;

    @Override
    public int layoutRes() {
        return R.layout.activity_done;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tv_done.setTitleText("意见反馈");
        iv_image.setImageResource(R.drawable.feedback_done);
        tv_info.setText("提交成功");
        tv_detail.setText("感谢您对我们工作的支持");
    }
}
