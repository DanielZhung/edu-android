package cn.zxd.edu.ui;

import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.FindCallback;
import com.avos.avoscloud.SaveCallback;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnItemClick;
import butterknife.OnItemLongClick;
import butterknife.OnTextChanged;
import cn.zxd.edu.R;
import cn.zxd.edu.adapter.CommonAdapter;
import cn.zxd.edu.adapter.CommonViewHolder;
import cn.zxd.edu.bean.Device;
import cn.zxd.edu.manager.DeviceManager;
import cn.zxd.edu.manager.SchoolManager;
import cn.zxd.edu.manager.UserManager;
import cn.zxd.edu.util.AVUtils;
import cn.zxd.edu.util.KeyboardUtils;
import cn.zxd.edu.util.SharedPreferencesHelper;
import cn.zxd.edu.util.SuperHttpUtil;
import cn.zxd.edu.view.TitleView;
import cz.msebera.android.httpclient.Header;

public class DeviceFragment extends BaseFragment {

    @BindView(R.id.tv_device)
    TitleView tv_device;

    @BindView(R.id.lv_device)
    ListView lv_device;

    @BindView(R.id.lv_device_admin)
    ListView lv_device_admin;

    @BindView(R.id.rl_school)
    RelativeLayout rl_school;

    @BindView(R.id.tv_school)
    TextView tv_school;

    @BindView(R.id.ll_device_empty)
    LinearLayout ll_device_empty;

    @BindView(R.id.ll_search)
    LinearLayout ll_search;

    @BindView(R.id.et_search)
    EditText et_search;

    @BindView(R.id.rg_dispatch)
    RadioGroup rg_dispatch;

    private List<Device> devices = new ArrayList<>();

    private List<Device> devicesShow = new ArrayList<>();

    private List<Device> devicesAdmin = new ArrayList<>();

    @Override
    public int layoutRes() {
        return R.layout.fragment_device;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        tv_device.setOnRightClickListener(new TitleView.OnRightClickListener() {
            @Override
            public void onRightClick(View view) {
                switch (view.getId()) {
                    case R.id.iv_titleViewRight:
                        addDevice();
                        break;
                    case R.id.tv_titleViewRight:
                        switch (rg_dispatch.getCheckedRadioButtonId()) {
                            case R.id.rb_on:
                                new AlertDialog.Builder(getContext()).setMessage("您确认要关闭所有已开启的设备吗？\n温馨提示:每日固定开启、关闭设备能有效提升机器使用寿命哦~")
                                        .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                for (final Device device : devicesShow) {
                                                    SuperHttpUtil.post("api/v1/device/control", new RequestParams("device", device.getName(), "index", "8"), new TextHttpResponseHandler() {
                                                        @Override
                                                        public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

                                                        }

                                                        @Override
                                                        public void onSuccess(int statusCode, Header[] headers, String responseString) {
                                                            new SharedPreferencesHelper(getContext(), "Device_Control").put(device.getDeviceId() + "-on", System.currentTimeMillis());
                                                        }
                                                    });
                                                }
                                            }
                                        })
                                        .setNegativeButton("取消", null).create().show();
                                break;
                            case R.id.rb_off:
                                new AlertDialog.Builder(getContext()).setMessage("您确认要一键开启所有未开机的设备吗？\n温馨提示:每日固定开启、关闭设备能有效提升机器使用寿命哦~")
                                        .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                for (final Device device : devicesShow) {
                                                    SuperHttpUtil.post("api/v1/device/control", new RequestParams("device", device.getName(), "index", "8"), new TextHttpResponseHandler() {
                                                        @Override
                                                        public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

                                                        }

                                                        @Override
                                                        public void onSuccess(int statusCode, Header[] headers, String responseString) {
                                                            new SharedPreferencesHelper(getContext(), "Device_Control").put(device.getDeviceId() + "-off", System.currentTimeMillis());
                                                        }
                                                    });
                                                }
                                            }
                                        })
                                        .setNegativeButton("取消", null).create().show();
                                break;
                        }
                        break;
                }

            }
        });
        lv_device.setEmptyView(ll_device_empty);
        lv_device.setAdapter(new CommonAdapter<Device>(getContext(), devices, R.layout.view_device_list) {
            @Override
            public void convert(CommonViewHolder helper, Device item) {
                helper.setText(R.id.tv_device_name, item.getDescription());
                helper.setText(R.id.tv_device_detail, item.getName());
                TextView tv_device_status = helper.getView(R.id.tv_device_status);
                tv_device_status.setText(item.isOnline() ? "在线" : "未开启");
            }
        });
        lv_device_admin.setEmptyView(ll_device_empty);
        lv_device_admin.setAdapter(new CommonAdapter<Device>(getContext(), devicesShow, R.layout.view_device_admin_list) {
            @Override
            public void convert(CommonViewHolder helper, Device item) {
                helper.setText(R.id.tv_description, item.getDescription());
                helper.setText(R.id.tv_device_name, item.getName());
                helper.setText(R.id.tv_online, item.isOnline() ? "在线" : "未开启");
                helper.setText(R.id.tv_on, item.isOn() ? "开机" : "关机");
                helper.getView(R.id.tv_on).setBackgroundColor(item.isOn() ? Color.GREEN : Color.RED);
            }
        });
        initSchool();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (rl_school != null) {
            initSchool();
        }
        if (getActivity() != null) {
            KeyboardUtils.closeKeybord(getActivity());
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && rl_school != null) {
            initSchool();
        }
        if (getActivity() != null) {
            KeyboardUtils.closeKeybord(getActivity());
        }
    }

    @OnClick(R.id.btn_addDevice)
    protected void addDevice() {
        AVUser user = AVUser.getCurrentUser();
        if (user == null) {
            startActivity(LoginActivity.class);
        } else if (!AVUtils.isAdministrator(user) && !AVUtils.isSuperAdministrator(user)) {
            new AlertDialog.Builder(getContext())
                    .setTitle("提示")
                    .setMessage("您不是学校管理员，请联系管理员进行设备添加")
                    .setPositiveButton("知道了", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    })
                    .create().show();
        } else {
            startActivity(AddDeviceActivity.class);
        }
    }

    @OnItemClick(R.id.lv_device)
    protected void deviceItemClick(AdapterView<?> parent, View view, int position, long id) {
        Device device = devices.get(position);
        if (device.isOnline()) {
            Bundle bundle = new Bundle();
            bundle.putSerializable("device", device);
            startActivity(DeviceControlActivity.class, bundle);
        } else {
            showToast("设备不在线，请检查");
        }
    }

    @OnItemClick(R.id.lv_device_admin)
    protected void deviceAdminItemClick(AdapterView<?> parent, View view, int position, long id) {
        Device device = devicesShow.get(position);
        if (device.isOnline()) {
            Bundle bundle = new Bundle();
            bundle.putSerializable("device", device);
            startActivity(DeviceControlActivity.class, bundle);
        } else {
            showToast("设备不在线，请检查");
        }
    }

    @OnItemLongClick(R.id.lv_device)
    protected boolean deviceItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        final Device device = devices.get(position);
        new AlertDialog.Builder(getActivity()).setTitle("删除设备")
                .setMessage("确定要删除设备:" + device.getDescription() + "(" + device.getName() + ")吗？")
                .setPositiveButton("删除", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        DeviceManager.getInstance().deleteDevice(device, new SaveCallback() {
                            @Override
                            public void done(AVException e) {
                                showToast("删除成功");
                                initDevices(AVUser.getCurrentUser());
                                UserManager.getInstance().deleteAllAuthorize(device.getDeviceId());
                            }
                        });
                    }
                })
                .setNegativeButton("取消", null).create().show();
        return true;
    }

    @OnItemLongClick(R.id.lv_device_admin)
    protected boolean deviceAdminItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        final Device device = devicesShow.get(position);
        new AlertDialog.Builder(getActivity()).setTitle("删除设备")
                .setMessage("确定要删除设备:" + device.getDescription() + "(" + device.getName() + ")吗？")
                .setPositiveButton("删除", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        DeviceManager.getInstance().deleteDevice(device, new SaveCallback() {
                            @Override
                            public void done(AVException e) {
                                showToast("删除成功");
                                initDevices(AVUser.getCurrentUser());
                                UserManager.getInstance().deleteAllAuthorize(device.getDeviceId());
                            }
                        });
                    }
                })
                .setNegativeButton("取消", null).create().show();
        return true;
    }

    @OnTextChanged(value = R.id.et_search, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    protected void afterTextChanged(Editable s) {
        selectDevice(s.toString());
    }

    @OnClick({R.id.rb_all, R.id.rb_on, R.id.rb_off})
    protected void radioButtonClick() {
        selectDevice(et_search.getText().toString());
    }

    private void initSchool() {
        final AVUser user = AVUser.getCurrentUser();
        if (user == null || user.getInt("SchoolIndex") <= 0) {
            rl_school.setVisibility(View.GONE);
            devices.clear();
            tv_device.setRightImageRes(devices.size() > 0 ? R.drawable.add_device_icon : 0);
            ((BaseAdapter) lv_device.getAdapter()).notifyDataSetChanged();
        } else {
            initDevices(user);
            SchoolManager.getInstance().getSchoolList(new FindCallback<AVObject>() {
                @Override
                public void done(List<AVObject> list, AVException e) {
                    for (AVObject object : list) {
                        if (object.getInt("Index") == user.getInt("SchoolIndex")) {
                            rl_school.setVisibility(View.VISIBLE);
                            tv_school.setText(String.format("当前学校：%s", object.getString("Name")));
                            break;
                        }
                    }
                }
            });
        }
    }

    private void initDevices(AVUser user) {
        et_search.setText("");
        if (AVUtils.isSuperAdministrator(user) || AVUtils.isAdministrator(user)) {  //查询所有以绑定的设备
            lv_device.setVisibility(View.GONE);
            ll_search.setVisibility(View.VISIBLE);
            rg_dispatch.setVisibility(View.VISIBLE);
            DeviceManager.getInstance().getBindDeviceList(new FindCallback<AVObject>() {
                @Override
                public void done(List<AVObject> list, AVException e) {
                    if (e == null) {
                        devicesAdmin.clear();
                        for (AVObject object : list) {
                            devicesAdmin.add(new Device(object.getString("Name"), object.getObjectId(), object.getString("Description"), object.getBoolean("Online"), object.getBoolean("On"), object.getInt("SchoolIndex")));
                        }
                        selectDevice("");
                    }
                }
            }, user.getInt("SchoolIndex"), true);
        } else {    //查询绑定关系表
            lv_device_admin.setVisibility(View.GONE);
            ll_search.setVisibility(View.GONE);
            rg_dispatch.setVisibility(View.GONE);
            DeviceManager.getInstance().getDeviceListByUser(new FindCallback<AVObject>() {
                @Override
                public void done(List<AVObject> list, AVException e) {
                    if (e == null) {
                        devices.clear();
                        for (AVObject object : list) {
                            devices.add(new Device(object.getString("Name"), object.getObjectId(), object.getString("Description"), object.getBoolean("Online"), object.getBoolean("On"), object.getInt("SchoolIndex")));
                        }
                        tv_device.setRightImageRes(devices.size() > 0 ? R.drawable.add_device_icon : 0);
                        ((BaseAdapter) lv_device.getAdapter()).notifyDataSetChanged();
                    }
                }
            }, user.getMobilePhoneNumber(), true);
        }
    }

    private void selectDevice(String string) {
        devicesShow.clear();
        //
        for (Device device : devicesAdmin) {
            switch (rg_dispatch.getCheckedRadioButtonId()) {
                case R.id.rb_all:
                    if (device.getDescription().contains(string)) {
                        devicesShow.add(device);
                    }
                    tv_device.setRightImageRes(devicesShow.size() > 0 ? R.drawable.add_device_icon : 0);
                    tv_device.setRightText("");
                    break;
                case R.id.rb_on:
                    if (device.isOn() && device.getDescription().contains(string)) {
                        devicesShow.add(device);
                    }
                    tv_device.setRightImageRes(0);
                    tv_device.setRightText("一键关机");
                    break;
                case R.id.rb_off:
                    if (!device.isOn() && device.getDescription().contains(string)) {
                        devicesShow.add(device);
                    }
                    tv_device.setRightImageRes(0);
                    tv_device.setRightText("一键开机");
                    break;
            }
        }
        ((BaseAdapter) lv_device_admin.getAdapter()).notifyDataSetChanged();
    }
}
